<?php
//app/Helpers/Envato/User.php
namespace App\Helpers\Integra;
 
use Illuminate\Support\Facades\DB;
 
class General {
    /**
     * @param int $user_id User-id
     * 
     * @return string
     */

	public static function fooBar() 
	   {
	      return \App\Helper\Integra\CustomHelper::fooBar();
	   }

	
	public static function renderResponse() 
	{
	  return \App\Helper\Integra\CustomHelper::renderResponse();
	}

	public static function setOnline($token, $user_id)
	{
	  return \App\Helper\Integra\CustomHelper::setOnline($user_id);
	}

	public static function setOffline($user_id)
	{
	  return \App\Helper\Integra\CustomHelper::setOffline($user_id);
	}

	public static function counterUser($user_id)
	{
		return \App\Helper\Integra\CustomHelper::counterUser($user_id);
	}
	
	public static function saveMongo()
	{
		return \App\Helper\Integra\CustomHelper::saveMongo();
	}

	public static function getMongo()
	{
		return \App\Helper\Integra\CustomHelper::getMongo();
	}
}