<?php

use MongoDB\Client as Mongo;
use App\Model\Master\User;

function renderResponse($result, $success, $message){
    $output = array();
    $output['Result'] = $result;
    $output['Success'] = $success;
    $output['Message'] = $message;
    return response()->json($output);
}

function setOnline($token, $user_id){    
    //update or create data
    $condition = ['user_id' => $user_id];
    $log_data = ['user_id' => $user_id, 'last_seen' => date('Y-m-d H:i:s'), 'remember_token' => $token, 'status' => 'online'];
    $DbMongo = 'users_online';
    $DbCollections = date('Y_m_d');
    $collection = \Mongo::get()->$DbMongo->$DbCollections;

    $find = $collection->findOne($condition);

    if($find){
        return $collection->updateMany(
            [ 'user_id' => $user_id ],
            [ '$set' => [ 'status' => 'online', 'remember_token' => $token ]]
        );
    }

    return $collection->insertOne($log_data);
}

function setOffline($user_id){
    $DbMongo = 'users_online';
    $DbCollections = date('Y_m_d');
    $collection = \Mongo::get()->$DbMongo->$DbCollections;

    $update = $collection->updateMany(
        [ 'user_id' => $user_id ],
        [ '$set' => [ 'status' => 'offline', 'remember_token' => null ]]
    );
}

function getUserInfo($input_token){
    //get user info from input token
    //return as object untuk bisa ambil hidden properti contoh id_users atau id_organisasi_perusahaan
    
    $user = User::where('remember_token', $input_token)->first();

    if(!is_null($user)){
        return $user;
    }

    return false;
}

function counterUser($user_id){
    $log_data = ['user_id' => $user_id, 'login_time' => date('Y-m-d H:i:s')];
    $DbMongo = 'users_visitor';
    $DbCollections = date('Y_m_d');
    $collection = \Mongo::get()->$DbMongo->$DbCollections;

    return $collection->insertOne($log_data);  
}

function saveMongo($data, $dbMongo, $collection){

    $collections = \Mongo::get()->$dbMongo->$collection;

    return $collections->insertOne($data);
}

function getMongo($cond, $dbMongo, $collection){
	
    $collections = \Mongo::get()->$dbMongo->$collection;

	return json_encode($collections->find($cond)->toArray());
}

function updateMongo(){

}

function delMongo(){

}