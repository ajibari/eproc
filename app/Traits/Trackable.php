<?php 

namespace App\Traits;

use App\Model\Master\User;
use Illuminate\Support\Facades\DB;
use MongoDB\Client as Mongo;
use Illuminate\Support\Facades\Log;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Contracts\Encryption\DecryptException;
use Illuminate\Support\Arr;

trait Trackable
{
    
    public static function bootTrackable()
    {
        
        static::retrieved(function ($model) {
                        

        });

        static::creating(function ($model) {

            $request = request();
            $input = $request->header();

            if (isset($input['token'])){
                $auth = User::where('remember_token', $input['token'])->first();
                $ip_address = $request->ip();
                
                if(!is_null($auth)){
                    $model->created_by = $auth->id;
                    $model->alamat_ip = $ip_address;    
                }
                
            }

            static::saveQuery($input, $request, $model);

        });

        static::updating(function ($model) {

            $request = request();
            $input = $request->header();

            if (isset($input['token'])){
                $auth = User::where('remember_token', $input['token'])->first();
                $ip_address = $request->ip();
                
                if(!is_null($auth)){
                    $model->updated_by = $auth->id;
                    $model->alamat_ip = $ip_address;
                }

                static::saveQuery($input, $request, $model);
            }
        });

        static::deleting(function ($model) {
            
            $request = request();
            $input = $request->header();

            static::saveQuery($input, $request, $model);

        });
    }

    public static function saveQuery($input, $request, $model)
    {
        if(!empty($input['token'])){
            $user = User::where('remember_token', $input['token'])->first();
            $ip_address = $request->ip();

            DB::listen(function ($query) use ($user, $ip_address) {
                $query_action = explode(' ',trim($query->sql));
                $conditions = ['update','insert','delete'];

                if(in_array($query_action[0], $conditions)){
                    if(!empty($user->id)){

                        if(env("APP_DEBUG") == true){
                            //insert into laravel log
                            Log::info($query->sql . ' - ' . serialize($query->bindings) . ' >>> '.$query->time.' ms >>> user_id:'.$user->id);
                        }

                        $log_data = ['user_id' => $user->id, 'action' => $query->sql.' - '.serialize($query->bindings).' exec_time '.$query->time, 'date' => date('Y-m-d H:i:s'), 'ip_address' => $ip_address];

                        $DbMongo = 'DB_'.date('Y_m_d');
                        $DbCollections = 'Query';
                        $collection = \Mongo::get()->$DbMongo->$DbCollections;

                        return $collection->insertOne($log_data);
                    }
                }
            });
        }
    }

    public function getForeignKeys(){
        return $this->foreignKeys;
    }

    public function encodeForeignKeys($model, $input, $method = null){

        $foreignKeys = is_null($model->getForeignKeys())?[]:$model->getForeignKeys();
        $primaryKey = $model->getKeyName();
        $result = [];
        $result['input'] = [];
        $result['unhashed'] = [];

        $result['input'] = array_merge($result['input'], $input);

        foreach($foreignKeys as $f => $v){
            $id = $this->decode($input[$v]);
            if(!is_null($id) || !$id){
                $unhashed[$v] = $id;
                unset($input[$v]);
            }
        }

        //unhashed primary key except for create and update method
        if(is_null($method)){
            $id = $this->decode($input[$primaryKey]);
            unset($input[$primaryKey]);
            if(!is_null($id) || !$id){
                $unhashed[$primaryKey] = $id;
            }
        }

        $result['unhashed'] = $unhashed;
        $result['unhashed'] = array_merge($result['unhashed'], $input);
        
        return $result;
    }

    public function encode($input){
      return Crypt::encryptString($input);
    }

    public function decode($input){
        
        try {
            $decrypted = Crypt::decryptString($input);
        } catch (DecryptException $e) {
            //jika input tidak bisa di decrypt (kode tidak dikenal)
            $decrypted = $e->getMessage();
        }

        return $decrypted;
    }

    public function decryptList($data, $list){
        $input = Arr::only($data, $list);

        foreach($input as $k => $v){
            $data[$k] = $this->decode($v);
        }

        return $data;
    }
}