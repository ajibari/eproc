<?php 

namespace App\Traits;

trait ExampleCode
{
    public function printThis()
    {
        echo "Trait executed";
        echo "<br/>";
        $this->eco();
        dd($this);
    }

    public function anotherMethod()
    {
        echo "Trait – anotherMethod() executed";
    }

    public function eco(){
    	echo "eco doang";
    }
}