<?php 

namespace App\Traits;

use App\Model\Master\User;
use Illuminate\Support\Facades\DB;
use MongoDB\Client as Mongo;
use Illuminate\Support\Facades\Log;
use Illuminate\Http\Request;
use Hashids;

trait RepositoryPattern
{
    public function getX (function($model)
    {
        $result = $model::with('provinsi')->get();
        $message = env('RESPONSE_GET_SUCCESS');

        if(!isset($result[0]->id_kota)){
            $message = env('RESPONSE_GET_FAILED');
            $result = null;
            return renderResponse($result, false, $message);
        }

        $message = env('RESPONSE_GET_DATA');
        return renderResponse($result, true, $message);
    });

    public function get_by(Request $request, $parameter){

    }

    public function get_by_many(){

    }
}