<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Trackable;


class MyModel extends Model
{

    use Trackable;

    public function delCheckRelationship($model){
        
        try {
           $model->delete();

        } catch (\Illuminate\Database\QueryException $e) {

            $db_code = config('app.db_code');
            $codeError = $e->errorInfo[0];

            //baca config dari config/app.php
                foreach ($db_code as $value) {
             
                    $key = array_keys($db_code);
                    $index = key($value);
                        
                    if(in_array($codeError, $value)){

                        switch ($key[$index]) {
                            case 'foreign key problem':
                                return $message = env('RESPONSE_DEL_FAILED_FOREIGNKEY_PROBLEM');
                                break;
                            
                            default:
                                return $message = env('RESPONSE_DEL_FAILED');
                                break;
                        }

                    }
                }
                
        } catch (PDOException $e) {
            dd($e->message);
        }  

        return true;
    }

    public function updateCheckRelationship($model){
        
        try {
           $model->save();

        } catch (\Illuminate\Database\QueryException $e) {

            $db_code = config('app.db_code');
            $codeError = $e->errorInfo[0];

            foreach ($db_code as $value) {
         
                $key = array_keys($db_code);
                $index = key($value);
                    
                if(in_array($codeError, $value)){
                    return $key[$index];
                }
            }
            
        } catch (PDOException $e) {
            dd($e->message);
        }  
    }

    public function getIdAttribute()
    {
        $keys = is_null($this->getForeignKeys())?[]:$this->getForeignKeys();
        $primaryKey = $this->getKeyName();
        array_push($keys, $primaryKey);

        foreach($keys as $f => $v){
            $unhashed[$v] = $this->encode($this->getAttributeValue($v));
        }

        return $unhashed;

    }
}
