<?php

namespace App\Model\Anggaran;

use App\Model\MyModel;
use App\Traits\Trackable;

class Anggaran extends MyModel
{
    use Trackable;

    protected $table = 'proc_anggaran_sumberdaya';
    protected $primaryKey = 'id_anggaran';

    protected $fillable = ['id_organisasi_perusahaan','kode_sumberdaya','id_bidang_usaha','id_bidang_usaha_detail','nama_item','spesifikasi_item','volume','harga_satuan','id_satuan','id_mata_uang'];
    protected $hidden = ['id_anggaran','id_organisasi_perusahaan','id_bidang_usaha','id_bidang_usaha_detail','id_satuan','id_mata_uang'];
    protected $foreignKeys = ['id_organisasi_perusahaan','id_bidang_usaha','id_bidang_usaha_detail','id_satuan','id_mata_uang'];
    protected $appends = ['id'];

    public function organisasi_perusahaan(){
        return $this->belongsTo('App\Model\Master\OrganisasiPerusahaan', 'id_organisasi_perusahaan');
    }

    public function bidang_usaha(){
        return $this->belongsTo('App\Model\Master\BidangUsaha', 'id_bidang_usaha');
    }

    public function bidang_usaha_detail(){
        return $this->belongsTo('App\Model\Master\BidangUsahaDetail', 'id_bidang_usaha_detail');
    }

    public function satuan(){
        return $this->belongsTo('App\Model\Master\Satuan', 'id_satuan');
    }

    public function mata_uang(){
        return $this->belongsTo('App\Model\Master\MataUang', 'id_mata_uang');
    }
}
