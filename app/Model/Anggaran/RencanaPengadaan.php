<?php

namespace App\Model\Anggaran;

use App\Model\MyModel;
use App\Traits\Trackable;

class RencanaPengadaan extends MyModel
{
    use Trackable;

    protected $table = 'proc_pengadaan';
    protected $primaryKey = 'id_pengadaan';

    protected $fillable = ['id_organisasi_perusahaan', 'kode_rencana_pengadaan', 'jenis_pengadaan', 'nama_rencana_pengadaan', 'lokasi', 'tanggal_permintaan', 'tanggal_mulai_kebutuhan', 'tanggal_selesai_kebutuhan', 'status', 'approved_by', 'tanggal_status'];
    protected $hidden = ['id_organisasi_perusahaan', 'id_pengadaan'];
    protected $foreignKeys = ['id_organisasi_perusahaan'];
    protected $appends = ['id'];

    public function item()
    {
        return $this->hasMany('App\Model\Anggaran\RencanaPengadaanItem', 'id_pengadaan');
    }

    public function organisasi(){
        return $this->belongsTo('App\Model\Master\OrganisasiPerusahaan', 'id_organisasi_perusahaan');
    }
}
