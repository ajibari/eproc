<?php

namespace App\Model\Anggaran;

use App\Model\MyModel;
use App\Traits\Trackable;

class PermintaanPesananItem extends MyModel
{
	use Trackable;

    protected $table = 'proc_permintaan_pesanan_item';
    protected $primaryKey = 'id_permintaan_pesanan_item';

    protected $fillable = ['id_permintaan_pesanan', 'kode_sumberdaya', 'id_bidang_usaha', 'id_bidang_usaha_detail', 'nama_item', 'spesifikasi_item', 'volume', 'harga_satuan', 'id_satuan', 'id_mata_uang'];
    protected $hidden = ['id_permintaan_pesanan_item', 'id_permintaan_pesanan', 'id_bidang_usaha', 'id_bidang_usaha_detail', 'id_satuan', 'id_mata_uang'];
    protected $foreignKeys = ['id_bidang_usaha', 'id_bidang_usaha_detail', 'id_satuan', 'id_mata_uang', 'id_permintaan_pesanan'];
    protected $appends = ['id'];

    public function pesanan()
    {
        return $this->BelongsTo('App\Model\Anggaran\PermintaanPesanan', 'id_permintaan_pesanan');
    }
}

