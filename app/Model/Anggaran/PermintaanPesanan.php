<?php

namespace App\Model\Anggaran;

use App\Model\MyModel;
use App\Traits\Trackable;

class PermintaanPesanan extends MyModel
{
    use Trackable;

    protected $table = 'proc_permintaan_pesanan';
    protected $primaryKey = 'id_permintaan_pesanan';

    protected $fillable = ['id_organisasi_perusahaan', 'kode_permintaan_pesanan', 'nama_permintaan_pesanan', 'lokasi', 'tanggal_permintaan', 'tanggal_kebutuhan', 'status', 'tanggal_status', 'requested_by', 'supervisor'];
    protected $hidden = ['id_permintaan_pesanan','id_organisasi_perusahaan'];
    protected $foreignKeys = ['id_organisasi_perusahaan'];
    protected $appends = ['id'];

    public function item()
    {
        return $this->hasMany('App\Model\Anggaran\PermintaanPesananItem', 'id_permintaan_pesanan');
    }

    public function organisasi_perusahaan(){
        return $this->belongsTo('App\Model\Master\OrganisasiPerusahaan', 'id_organisasi_perusahaan');
    }
}
