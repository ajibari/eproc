<?php

namespace App\Model\Vendor;

use Illuminate\Database\Eloquent\Model;
use App\Model\MyModel;
use App\Traits\Trackable;


class RekananTenagaAhli extends MyModel
{
    use Trackable;
	
    protected $table = 'rekanan_tenaga_ahli';
    protected $primaryKey = 'id_tenaga_ahli';

    protected $fillable = ['id_tenaga_ahli', 'id_rekanan', 'jenjang_pendidikan', 'jumlah'];
    protected $hidden = ['id_tenaga_ahli', 'id_rekanan'];
    protected $foreignKeys = [];
    protected $appends = ['id'];

    public function rekanan(){
        return $this->belongsTo('App\Model\Vendor\Rekanan', 'id_rekanan');
    }
    
}
