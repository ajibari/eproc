<?php

namespace App\Model\Vendor;

use App\Model\MyModel;
use App\Traits\Trackable;

class RekananDokumenPenawaran extends MyModel
{
    use Trackable;
	
    protected $table = 'rekanan_dokumen_penawaran';
    protected $primaryKey = 'id_rekanan_dokumen_penawaran';

    protected $fillable = ['id_lelang', 'id_rekanan', 'judul_file', 'file_path'];
    protected $hidden = ['id_lelang', 'id_rekanan'];
    protected $foreignKeys = [];
    protected $appends = ['id'];
	
	public function lelang(){
        return $this->belongsTo('App\Model\Lelang\Lelang', 'id_lelang');
    }
 
}
