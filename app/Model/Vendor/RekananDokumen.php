<?php

namespace App\Model\Vendor;

use Illuminate\Database\Eloquent\Model;
use App\Model\MyModel;
use App\Traits\Trackable;


class RekananDokumen extends MyModel
{
    use Trackable;
	
    protected $table = 'rekanan_dokumen';
    protected $primaryKey = 'id_rekanan_dokumen';

    protected $fillable = ['id_rekanan', 'id_dokumen_perusahaan', 'tanggal_mulai', 'tanggal_selesai', 'file_path', 'keterangan'];
    protected $hidden = ['id_rekanan', 'id_dokumen_perusahaan'];
    protected $foreignKeys = ['id_rekanan', 'id_dokumen_perusahaan'];
    protected $appends = ['id'];

    public function dokumen_perusahaan(){
        return $this->belongsTo('App\Model\Master\DokumenPerusahaan', 'id_dokumen_perusahaan');
    }

}
