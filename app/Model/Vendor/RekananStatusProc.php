<?php

namespace App\Model\Vendor;

use Illuminate\Database\Eloquent\Model;
use App\Model\MyModel;
use App\Traits\Trackable;


class RekananStatusProc extends MyModel
{
    use Trackable;
	
    protected $table = 'rekanan_status_proc';
    protected $primaryKey = 'id_rekanan_status_proc';

    protected $fillable = ['id_lelang', 'id_rekanan', 'daftar', 'tanggal_daftar', 'status_kualifikasi', 'status_kualifikasi_disetujui_ol', 'status_kualifikasi_tanggal', 'kode_bidding_penawaran', 'kode_bidding_penawaran_status', 'status_penawaran', 'status_penawaran_disetujui_oleh', 'status_penawaran_tanggal', 'kode_bidding_eauction', 'kode_bidding_eauction_status', 'status_nominasi'];
    protected $hidden = ['id_lelang', 'id_rekanan'];
    protected $foreignKeys = [];
    protected $appends = ['id'];

    public function rekanan(){
        return $this->belongsTo('App\Model\Vendor\Rekanan', 'id_rekanan');
    }

    public function lelang(){
        return $this->belongsTo('App\Model\Lelang\Lelang', 'id_lelang');
    }
    
}
