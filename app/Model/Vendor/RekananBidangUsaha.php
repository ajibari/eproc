<?php

namespace App\Model\Vendor;

use App\Model\MyModel;
use App\Traits\Trackable;


class RekananBidangUsaha extends MyModel
{
    use Trackable;
	
    protected $table = 'rekanan_bidang_usaha';
    protected $primaryKey = 'id_rekanan_bidang_usaha';

    protected $fillable = ['id_rekanan', 'id_bidang_usaha', 'id_bidang_usaha_detail', 'keterangan'];
    protected $hidden = ['id_rekanan', 'id_bidang_usaha', 'id_rekanan_bidang_usaha', 'id_bidang_usaha_detail'];
    protected $foreignKeys = ['id_rekanan', 'id_bidang_usaha', 'id_bidang_usaha_detail'];
    protected $appends = ['id'];

    public function rekanan(){
        return $this->belongsTo('App\Model\Vendor\Rekanan', 'id_rekanan');
    }

    public function bidang_usaha(){
        return $this->belongsTo('App\Model\Master\BidangUsaha', 'id_rekanan');
    }

    public function bidang_usaha_detail(){
    	return $this->belongsTo('App\Model\Master\BidangUsahaDetail', 'id_bidang_usaha_detail');
    }

}
