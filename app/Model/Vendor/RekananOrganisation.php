<?php

namespace App\Model\Vendor;

use Illuminate\Database\Eloquent\Model;
use App\Model\MyModel;
use App\Traits\Trackable;


class RekananOrganisation extends MyModel
{
    use Trackable;
	
    protected $table = 'rekanan_org';
    protected $primaryKey = 'id_rekanan_org';

    protected $fillable = ['id_rekanan', 'id_organisasi_perusahaan'];
    protected $hidden = ['id_rekanan_org', 'id_rekanan'];
    protected $foreignKeys = ['id_rekanan', 'id_organisasi_perusahaan'];
    protected $appends = ['id'];

    public function rekanan(){
        return $this->belongsTo('App\Model\Vendor\Rekanan', 'id_rekanan');

    }

    public function organisasi(){
        return $this->belongsTo('App\Model\Master\OrganisasiPerusahaan', 'id_organisasi_perusahaan');
    }

}
