<?php

namespace App\Model\Vendor;

use Illuminate\Database\Eloquent\Model;
use App\Model\MyModel;
use App\Traits\Trackable;


class RekananPenawaranItem extends MyModel
{
    use Trackable;
	
    protected $table = 'rekanan_penawaran_item';
    protected $primaryKey = 'id_rekanan_penawaran_item';

    protected $fillable = ['id_lelang', 'id_rekanan', 'id_pengadaan_item', 'volume', 'uraian_item_alternatif', 'harga_satuan', 'bisa_nego', 'flag_jenis_penawaran'];
    protected $hidden = ['id_rekanan_penawaran_item', 'id_lelang', 'id_rekanan', 'id_pengadaan_item'];
    protected $foreignKeys = [];
    protected $appends = ['id'];

    public function lelang(){
        return $this->belongsTo('App\Model\Lelang\Lelang', 'id_lelang');
    }

    public function rekanan(){
        return $this->belongsTo('App\Model\Vendor\Rekanan', 'id_rekanan');
    }

    public function pengadaan_item(){
        return $this->belongsTo('App\Model\Anggaran\RencanaPengadaanItem', 'id_pengadaan_item');
    }

}
