<?php

namespace App\Model\Vendor;

use Illuminate\Database\Eloquent\Model;
use App\Model\MyModel;
use App\Traits\Trackable;


class RekananPenawaranAdditional extends MyModel
{
    use Trackable;
	
    protected $table = 'rekanan_penawaran_additional';
    protected $primaryKey = 'id_rekanan_additional_penawaran';

    protected $fillable = ['id_lelang', 'id_rekanan', 'uraian', 'harga'];
    protected $hidden = ['id_lelang', 'id_rekanan'];
    protected $foreignKeys = [];
    protected $appends = ['id'];

    public function rekanan(){
        return $this->belongsTo('App\Model\Vendor\Rekanan', 'id_rekanan');
    }

}
