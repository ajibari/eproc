<?php

namespace App\Model\Vendor;

use Illuminate\Database\Eloquent\Model;
use App\Model\MyModel;
use App\Traits\Trackable;


class RekananPic extends MyModel
{
    use Trackable;
	
    protected $table = 'rekanan_pic';
    protected $primaryKey = 'id_rekanan_pic';

    protected $fillable = ['id_user', 'id_rekanan', 'nama_depan', 'nama_belakang', 'jabatan', 'email', 'telepon', 'telepon_ext', 'mobile_phone', 'alamat'];
    protected $hidden = ['id_rekanan_pic', 'id_user', 'id_rekanan'];
    protected $foreignKeys = ['id_user', 'id_rekanan'];
    protected $appends = ['id'];

    public function user(){
        return $this->belongsTo('App\Model\Master\User', 'id_user');
    }

    public function rekanan(){
        return $this->belongsTo('App\Model\Vendor\Rekanan', 'id_rekanan');
    }
    
}
