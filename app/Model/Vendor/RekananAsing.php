<?php

namespace App\Model\Vendor;

use Illuminate\Database\Eloquent\Model;
use App\Model\MyModel;
use App\Traits\Trackable;


class RekananAsing extends MyModel
{
    use Trackable;
	
    protected $table = 'rekanan_asing';
    protected $primaryKey = 'id_rekanan_asing';

    protected $fillable = ['id_rekanan', 'cr_no', 'cr_exp', 'company_name', 'address_line1', 'address_line2', 'po_box', 'city', 'country', 'zip', 'telephone', 'fax', 'website', 'email'];
    protected $hidden = ['id_rekanan'];
    protected $foreignKeys = ['id_rekanan'];
    protected $appends = ['id'];

    public function rekanan(){
        return $this->belongsTo('App\Model\Vendor\Rekanan', 'id_rekanan');
    }

}
