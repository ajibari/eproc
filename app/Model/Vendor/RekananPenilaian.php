<?php

namespace App\Model\Vendor;

use Illuminate\Database\Eloquent\Model;
use App\Model\MyModel;
use App\Traits\Trackable;


class RekananPenilaian extends MyModel
{
    use Trackable;
	
    protected $table = 'rekanan_penilaian';
    protected $primaryKey = 'id_rekanan_penilaian';

    protected $fillable = ['id_lelang', 'id_rekanan', 'id_penilaian_lelang_detail', 'nilai', 'uraian'];
    protected $hidden = ['id_pengalaman', 'id_rekanan'];
    protected $foreignKeys = [];
    protected $appends = ['id'];

    public function rekanan(){
        return $this->belongsTo('App\Model\Vendor\Rekanan', 'id_rekanan');
    }

    public function penilaian_lelang_detail(){
    	return $this->belongsTo('App\Model\Lelang\PenilaianLelangDetail', 'id_penilaian_lelang_detail');
    }
    
}
