<?php

namespace App\Model\Vendor;

use Illuminate\Database\Eloquent\Model;
use App\Model\MyModel;
use App\Traits\Trackable;


class RekananLokal extends MyModel
{
    use Trackable;
	
    protected $table = 'rekanan_lokal';
    protected $primaryKey = 'id_rekanan_lokal';

    protected $fillable = ['id_rekanan', 'no_npwp', 'nama_perusahaan', 'id_provinsi', 'id_kota', 'id_kecamatan', 'detail_alamat', 'kode_pos', 'id_tipe_perusahaan', 'id_jenis_perusahaan', 'jenis_perusahaan_lainnya', 'id_cakupan_wilayah', 'id_kualifikasi_perusahaan', 'telepon', 'fax', 'website', 'email'];
    protected $hidden = ['id_rekanan_lokal', 'id_rekanan', 'id_provinsi', 'id_kota', 'id_kecamatan', 'id_tipe_perusahaan', 'id_jenis_perusahaan', 'id_cakupan_wilayah', 'id_kualifikasi_perusahaan'];
    protected $foreignKeys = ['id_rekanan', 'id_provinsi', 'id_kota', 'id_kecamatan', 'id_tipe_perusahaan', 'id_jenis_perusahaan', 'id_cakupan_wilayah', 'id_kualifikasi_perusahaan'];
    protected $appends = ['id'];

    public function rekanan(){
        return $this->belongsTo('App\Model\Vendor\Rekanan', 'id_rekanan');
    }

    public function provinsi(){
        return $this->belongsTo('App\Model\Master\Province', 'id_provinsi');
    }

    public function kota(){
        return $this->belongsTo('App\Model\Master\Kota', 'id_kota');
    }

    public function kecamatan(){
        return $this->belongsTo('App\Model\Master\Kecamatan', 'id_kecamatan');
    }

    public function tipe_perusahaan(){
        return $this->belongsTo('App\Model\Master\TipePerusahaan', 'id_tipe_perusahaan');
    }

    public function jenis_perusahaan(){
        return $this->belongsTo('App\Model\Master\JenisPerusahaan', 'id_jenis_perusahaan');
    }

    public function cakupan_wilayah(){
        return $this->belongsTo('App\Model\Master\CakupanWilayah', 'id_cakupan_wilayah');
    }

    public function kualifikasi_perusahaan(){
        return $this->belongsTo('App\Model\Master\KualifikasiPerusahaan', 'id_kualifikasi_perusahaan');
    }

}
