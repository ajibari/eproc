<?php

namespace App\Model\Vendor;

use Illuminate\Database\Eloquent\Model;
use App\Model\MyModel;
use App\Traits\Trackable;


class RekananApproval extends MyModel
{
    use Trackable;
	
    protected $table = 'rekanan_approval';
    protected $primaryKey = 'id_rekanan_approval';

    protected $fillable = ['id_rekanan', 'keterangan', 'status', 'id_global_setting_config', 'suspend', 'keterangan_suspend'];
    protected $hidden = ['id_rekanan', 'id_global_setting_config'];
    protected $foreignKeys = ['id_rekanan', 'id_global_setting_config'];
    protected $appends = ['id'];

    public function rekanan(){
        return $this->belongsTo('App\Model\Vendor\Rekanan', 'id_rekanan');
    }

    public function rekanan_lokal(){
        return $this->belongsTo('App\Model\Vendor\RekananLokal', 'id_rekanan');
    }

    public function rekanan_asing(){
        return $this->belongsTo('App\Model\Vendor\RekananAsing', 'id_rekanan');
    }

    public function rekanan_org(){
        return $this->hasMany('App\Model\Vendor\RekananOrganisation', 'id_rekanan');
    }

    public function global_setting_config(){
        return $this->belongsTo('App\Model\Master\GlobalSettingConfig', 'id_global_setting_config');
    }
}
