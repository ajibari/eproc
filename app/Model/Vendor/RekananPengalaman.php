<?php

namespace App\Model\Vendor;

use Illuminate\Database\Eloquent\Model;
use App\Model\MyModel;
use App\Traits\Trackable;


class RekananPengalaman extends MyModel
{
    use Trackable;
	
    protected $table = 'rekanan_pengalaman';
    protected $primaryKey = 'id_pengalaman';

    protected $fillable = ['id_rekanan', 'instansi', 'bidang_usaha', 'tanggal_mulai', 'tanggal_selesai', 'keterangan'];
    protected $hidden = ['id_pengalaman', 'id_rekanan'];
    protected $foreignKeys = [];
    protected $appends = ['id'];

    public function rekanan(){
        return $this->belongsTo('App\Model\Vendor\Rekanan', 'id_rekanan');
    }
    
}
