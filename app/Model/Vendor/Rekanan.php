<?php

namespace App\Model\Vendor;

use Illuminate\Database\Eloquent\Model;
use App\Model\MyModel;
use App\Traits\Trackable;

class Rekanan extends MyModel
{
    use Trackable;
	
    protected $table = 'rekanan';
    protected $primaryKey = 'id_rekanan';

    protected $fillable = ['perusahaan_lokal', 'kode_rekanan'];
    protected $hidden = ['id_rekanan'];
    protected $foreignKeys = [];
    protected $appends = ['id'];

    public function rekanan_lokal(){
        return $this->hasOne('App\Model\Vendor\RekananLokal', 'id_rekanan');
    }

    public function rekanan_asing(){
        return $this->hasOne('App\Model\Vendor\RekananAsing', 'id_rekanan');
    }

    public function rekanan_bidang_usaha(){
        return $this->hasMany('App\Model\Vendor\RekananBidangUsaha', 'id_rekanan');
    }

    public function rekanan_pic(){
        return $this->hasMany('App\Model\Vendor\RekananPic', 'id_rekanan');
    }

    public function rekanan_dokumen(){
        return $this->hasMany('App\Model\Vendor\RekananDokumen', 'id_rekanan');
    }

    public function rekanan_org(){
        return $this->hasMany('App\Model\Vendor\RekananOrganisation', 'id_rekanan');
    }

    public function rekanan_approval(){
        return $this->hasMany('App\Model\Vendor\RekananApproval', 'id_rekanan');
    }

}
