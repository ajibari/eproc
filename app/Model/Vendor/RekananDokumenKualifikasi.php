<?php

namespace App\Model\Vendor;

use Illuminate\Database\Eloquent\Model;
use App\Model\MyModel;
use App\Traits\Trackable;


class RekananDokumenKualifikasi extends MyModel
{
    use Trackable;
	
    protected $table = 'rekanan_dokumen_kualifikasi';
    protected $primaryKey = 'id_rekanan_kualifikasi';

    protected $fillable = ['id_lelang', 'id_rekanan', 'judul_file', 'file_path'];
    protected $hidden = ['id_rekanan_kualifikasi', 'id_lelang', 'id_rekanan'];
    protected $foreignKeys = ['id_lelang', 'id_rekanan'];
    protected $appends = ['id'];

    public function lelang(){
        return $this->belongsTo('App\Model\Lelang\Lelang', 'id_lelang');
    }

    public function rekanan(){
        return $this->belongsTo('App\Model\Vendor\Rekanan', 'id_rekanan');
    }

}
