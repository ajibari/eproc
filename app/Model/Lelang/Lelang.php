<?php

namespace App\Model\Lelang;

use App\Model\MyModel;
use App\Traits\Trackable;

class Lelang extends MyModel
{
    use Trackable;

    protected $table = 'proc_lelang';
    protected $primaryKey = 'id_lelang';

    protected $fillable = ['id_pengadaan', 'id_organisasi_perusahaan', 'nama_paket_lelang', 'jenis_lelang', 'kualifikasi_grade', 'grade', 'id_mata_uang', 'persentase_batas_kewajaran', 'pra_kualifikasi', 'perlu_aanwijzing', 'perlu_eauction', 'status_submit', 'status_approve', 'approved_by', 'tanggal_approve', 'status_broadcast'];
    protected $hidden = ['id_lelang', 'id_pengadaan', 'id_organisasi_perusahaan', 'id_mata_uang'];
    protected $foreignKeys = ['id_pengadaan', 'id_organisasi_perusahaan', 'id_mata_uang'];
    protected $appends = ['id'];

    public function pengadaan(){
        return $this->belongsTo('App\Model\Anggaran\RencanaPengadaan', 'id_pengadaan');
    }

    public function organisasi_perusahaan(){
        return $this->belongsTo('App\Model\Master\OrganisasiPerusahaan', 'id_organisasi_perusahaan');
    }

    public function mata_uang(){
        return $this->belongsTo('App\Model\Master\MataUang', 'id_mata_uang');
    }

    public function jadwal_tahapan_lelang(){
    	return $this->hasMany('App\Model\Lelang\JadwalTahapanLelang', 'id_lelang');
    }
}
