<?php

namespace App\Model\Lelang;

use App\Model\MyModel;
use App\Traits\Trackable;

class JadwalTahapanLelang extends MyModel
{
    use Trackable;

    protected $table = 'proc_jadwal_tahapan_lelang';
    protected $primaryKey = 'id_jadwal_tahapan_lelang';

    protected $fillable = ['id_lelang', 'id_tahapan_lelang', 'tanggal_mulai', 'tanggal_selesai'];
    protected $hidden = ['id_jadwal_tahapan_lelang', 'id_lelang', 'id_tahapan_lelang'];
    protected $foreignKeys = ['id_lelang', 'id_tahapan_lelang'];
    protected $appends = ['id'];

}
