<?php

namespace App\Model\Master;

use App\Model\MyModel;
use App\Traits\Trackable;

class Kecamatan extends MyModel
{
	use Trackable;

    protected $table = 'mst_kecamatan';
    protected $primaryKey = 'id_kecamatan';

    protected $fillable = ['id_kota', 'nama_kecamatan', 'id_provinsi'];
    protected $hidden = ['id_kecamatan','id_kota','id_provinsi'];
    protected $foreignKeys = ['id_provinsi','id_kota'];
    protected $appends = ['id'];
    
    public function kota(){
        return $this->belongsTo('App\Model\Master\Kota', 'id_kota');
    }

    public function provinsi(){
        return $this->belongsTo('App\Model\Master\Province', 'id_provinsi');
    }

}
