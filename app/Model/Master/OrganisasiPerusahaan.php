<?php

namespace App\Model\Master;

use App\Model\MyModel;
use App\Traits\Trackable;


class OrganisasiPerusahaan extends MyModel
{
    use Trackable;

    protected $table = 'mst_organisasi_perusahaan';
    protected $primaryKey = 'id_organisasi_perusahaan';

    protected $fillable = ['kode', 'kode_induk', 'kode_organisasi_perusahaan', 'nama_organisasi_perusahaan'];
    protected $hidden = ['id_organisasi_perusahaan'];
    protected $foreignKeys = [];
    protected $appends = ['id'];

}
