<?php

namespace App\Model\Master;

use App\Model\MyModel;
use Illuminate\Database\Eloquent\Model;
use App\Traits\Trackable;


class KriteriaPenilaian extends MyModel
{

	use Trackable;
	
    protected $table = 'mst_kriteria_penilaian';
    protected $primaryKey = 'id_kriteria_penilaian';

    protected $fillable = ['nama_kriteria_penilaian'];
    protected $hidden = ['id_kriteria_penilaian'];
    protected $foreignKeys = [];
    protected $appends = ['id'];

}
