<?php

namespace App\Model\Master;

use App\Model\MyModel;
use App\Traits\Trackable;

class Province extends MyModel
{
	use Trackable;

    protected $table = 'mst_provinsi';
    protected $primaryKey = 'id_provinsi';

    protected $fillable = ['nama_provinsi'];
    protected $hidden = ['id_provinsi'];
    protected $appends = ['id'];

}
