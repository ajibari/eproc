<?php

namespace App\Model\Master;

use App\Model\MyModel;
use Illuminate\Database\Eloquent\Model;
use App\Traits\Trackable;


class BidangUsaha extends MyModel
{

    use Trackable;

    protected $table = 'mst_bidang_usaha';
    protected $primaryKey = 'id_bidang_usaha';

    protected $fillable = ['nama_bidang_usaha'];
    protected $hidden = ['id_bidang_usaha'];
    protected $foreignKeys = [];
    protected $appends = ['id'];

    public function detail(){
        return $this->hasMany('App\Model\Master\BidangUsahaDetail', 'id_bidang_usaha');
    }
    
    // protected static function boot() {
    //     parent::boot();

    //     static::deleting(function($bidangusaha) { // before delete() method call this
    //          $bidangusaha->detail()->delete();
    //          // do the rest of the cleanup...
    //     });
    // }
}
