<?php

namespace App\Model\Master;

use App\Model\MyModel;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Trackable;


class Satuan extends MyModel
{
	use Trackable;
	
    protected $table = 'mst_satuan';
    protected $primaryKey = 'id_satuan';

    protected $fillable = ['nama_satuan'];
    protected $hidden = ['id_satuan'];
    protected $foreignKeys = [];
    protected $appends = ['id'];

}
