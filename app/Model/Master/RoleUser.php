<?php

namespace App\Model\Master;

use App\Model\MyModel;
use Illuminate\Database\Eloquent\Model;

class RoleUser extends MyModel
{
    //
    protected $table = 'role_user';
    protected $primaryKey = 'user_id';

    public function permission()
    {
        return $this->hasManyThrough(
            'App\Model\Master\Permission',
            	'App\Model\Master\PermissionRole',
            	'role_id',
            'id',
            'role_id',
            'permission_id'
        );
    }
}
