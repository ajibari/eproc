<?php

namespace App\Model\Master;

use App\Model\MyModel;
use Illuminate\Database\Eloquent\Model;
use App\Traits\Trackable;


class Jabatan extends MyModel
{
	use Trackable;

    protected $table = 'mst_jabatan';
    protected $primaryKey = 'id_jabatan';

    protected $fillable = ['nama_jabatan'];
    protected $hidden = ['id_jabatan'];
    protected $foreignKeys = [];
    protected $appends = ['id'];

}
