<?php

namespace App\Model\Master;

use App\Model\MyModel;
use App\Traits\Trackable;


class CakupanWilayah extends MyModel
{

	use Trackable;

    protected $table = 'mst_cakupan_wilayah';
    protected $primaryKey = 'id_cakupan_wilayah';

    protected $fillable = ['nama_cakupan_wilayah', 'keterangan'];
    protected $hidden = ['id_cakupan_wilayah'];
    protected $foreignKeys = [];
    protected $appends = ['id'];

}
