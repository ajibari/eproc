<?php

namespace App\Model\Master;

use App\Model\MyModel;

use Illuminate\Database\Eloquent\Model;

class PermissionRole extends MyModel
{
    //
    protected $table = 'permission_role';
}
