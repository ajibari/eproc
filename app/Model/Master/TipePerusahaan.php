<?php

namespace App\Model\Master;

use App\Model\MyModel;
use Illuminate\Database\Eloquent\Model;
use App\Traits\Trackable;


class TipePerusahaan extends MyModel
{

    use Trackable;

    protected $table = 'mst_tipe_perusahaan';
    protected $primaryKey = 'id_tipe_perusahaan';

    protected $fillable = ['nama_tipe_perusahaan'];
    protected $hidden = ['id_tipe_perusahaan'];
    protected $foreignKeys = [];
    protected $appends = ['id'];

}
