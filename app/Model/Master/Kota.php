<?php

namespace App\Model\Master;

use App\Model\MyModel;
use Illuminate\Database\Eloquent\Model;
use App\Traits\Trackable;


class Kota extends MyModel
{
	use Trackable;
	
    protected $table = 'mst_kota';
    protected $primaryKey = 'id_kota';

    protected $fillable = ['id_provinsi', 'nama_kota'];
    protected $hidden = ['id_kota', 'id_provinsi'];
    protected $foreignKeys = ['id_provinsi'];
    protected $appends = ['id'];

    public function provinsi(){
        return $this->belongsTo('App\Model\Master\Province', 'id_provinsi');
    }

}
