<?php

namespace App\Model\Master;

use App\Model\MyModel;
use App\Traits\Trackable;


class BidangUsahaDetail extends MyModel
{
	use Trackable;

    protected $table = 'mst_bidang_usaha_detail';
    protected $primaryKey = 'id_bidang_usaha_detail';

    protected $fillable = ['id_bidang_usaha', 'nama_bidang_usaha_detail'];
    protected $hidden = ['id_satuan', 'id_bidang_usaha_detail', 'id_bidang_usaha'];
    protected $foreignKeys = ['id_bidang_usaha'];
    protected $appends = ['id'];

    public function bidang_usaha(){
        return $this->belongsTo('App\Model\Master\BidangUsaha', 'id_bidang_usaha');
    }
}
