<?php

namespace App\Model\Master;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laratrust\Traits\LaratrustUserTrait;
use App\Traits\Trackable;
use App\Model\MyModel;
use Illuminate\Database\Eloquent\Model;

class User extends Authenticatable
{
    use Notifiable;
    use LaratrustUserTrait;
    use Trackable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'users';
    protected $primaryKey = 'id';

    protected $fillable = [
        'name', 'email', 'password', 'is_verified'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'id_organisasi_perusahaan', 'id', 'id_jabatan'
    ];
    
    protected $dates = ['deleted_at'];
    protected $foreignKeys = ['id_jabatan','id_organisasi_perusahaan'];
    protected $appends = ['secure'];
    
    public function role_user()
    {
        return $this->hasOne('App\RoleUser');
    }

    public function role()
    {
        return $this->hasManyThrough(
            'App\Model\Master\Role',
            'App\Model\Master\RoleUser',
            'user_id', // Foreign key on user role table... 
            'id', // Foreign key on role table...
            'id', // Local key on countries table...
            'role_id' // Local key on users table...
        );
    }

    public function organization(){
        return $this->hasMany('App\Model\Master\OrganisasiPerusahaan', 'id_organisasi_perusahaan');
    }

    public function jabatan(){
        return $this->hasMany('App\Model\Master\Jabatan', 'id_jabatan');
    }

    public function getSecureAttribute()
    {
        $keys = is_null($this->getForeignKeys())?[]:$this->getForeignKeys();
        $primaryKey = $this->getKeyName();
        array_push($keys, $primaryKey);
        foreach($keys as $f => $v){
            $unhashed[$v] = $this->encode($this->getAttributeValue($v));
        }
        // dd($unhashed);
        return $unhashed;

    }

}
