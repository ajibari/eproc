<?php

namespace App\Model\Master;

use App\Model\MyModel;
use Illuminate\Database\Eloquent\Model;
use App\Traits\Trackable;


class DokumenPerusahaan extends MyModel
{

	use Trackable;

    protected $table = 'mst_dokumen_perusahaan';
    protected $primaryKey = 'id_dokumen_perusahaan';

    protected $fillable = ['nama_dokumen_perusahaan'];
    protected $hidden = ['id_dokumen_perusahaan'];
    protected $foreignKeys = [];
    protected $appends = ['id'];

}
