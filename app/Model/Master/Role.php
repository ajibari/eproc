<?php

namespace App\Model\Master;

use Illuminate\Database\Eloquent\Model;
use Laratrust\Models\LaratrustRole;
use Illuminate\Database\Eloquent\SoftDeletes;

class Role extends LaratrustRole
{
    //
    use SoftDeletes;

    protected $dates = ['deleted_at'];

}
