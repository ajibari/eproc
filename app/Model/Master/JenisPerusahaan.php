<?php

namespace App\Model\Master;

use App\Model\MyModel;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Trackable;


class JenisPerusahaan extends MyModel
{
	use Trackable;

    protected $table = 'mst_jenis_perusahaan';
    protected $primaryKey = 'id_jenis_perusahaan';

    protected $fillable = ['nama_jenis_perusahaan'];
    protected $hidden = ['id_jenis_perusahaan'];
    protected $foreignKeys = [];
    protected $appends = ['id'];

}
