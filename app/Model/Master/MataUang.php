<?php

namespace App\Model\Master;

use App\Model\MyModel;
use App\Traits\Trackable;


class MataUang extends MyModel
{

    use Trackable;

    protected $table = 'mst_mata_uang';
    protected $primaryKey = 'id_mata_uang';

    protected $fillable = ['kode_mata_uang', 'nama_mata_uang'];
    protected $hidden = ['id_mata_uang'];
    protected $foreignKeys = [];
    protected $appends = ['id'];

}
