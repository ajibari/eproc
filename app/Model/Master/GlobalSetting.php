<?php

namespace App\Model\Master;

use App\Model\MyModel;
use Illuminate\Database\Eloquent\Model;
use App\Traits\Trackable;

class GlobalSetting extends MyModel
{
	use Trackable;

    protected $table = 'mst_global_setting';
    protected $primaryKey = 'id_global_setting';

    protected $fillable = ['nama_parameter', 'nilai_parameter', 'keterangan'];
    protected $hidden = ['id_global_setting'];
    protected $foreignKeys = [];
    protected $appends = ['id'];

    public function config(){
        return $this->hasMany('App\Model\Master\GlobalSettingConfig', 'id_global_setting');
    }
    
}
