<?php

namespace App\Model\Master;

use App\Model\MyModel;
use Illuminate\Database\Eloquent\Model;
use App\Traits\Trackable;


class KualifikasiPerusahaan extends MyModel
{

	use Trackable;

    protected $table = 'mst_kualifikasi_perusahaan';
    protected $primaryKey = 'id_kualifikasi_perusahaan';

    protected $fillable = ['nama_kualifikasi_perusahaan','keterangan'];
    protected $hidden = ['id_kualifikasi_perusahaan'];
    protected $foreignKeys = [];
    protected $appends = ['id'];

}
