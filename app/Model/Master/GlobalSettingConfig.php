<?php

namespace App\Model\Master;

use App\Model\MyModel;
use Illuminate\Database\Eloquent\Model;
use App\Traits\Trackable;

class GlobalSettingConfig extends MyModel
{
    use Trackable;

    protected $table = 'mst_global_setting_config';
    protected $primaryKey = 'id_global_setting_config';

    protected $fillable = ['id_global_setting', 'table_name', 'id_value', 'sequence', 'keterangan'];
    protected $hidden = ['id_global_setting_config', 'id_global_setting'];
    protected $foreignKeys = ['id_global_setting_config'];
    protected $appends = ['id'];

    public function global_setting(){
        return $this->belongsTo('App\Model\Master\GlobalSetting', 'id_global_setting');
    }

    public function organization(){
        return $this->belongsTo('App\Model\Master\OrganisasiPerusahaan', 'id_value');
    }
    
}
