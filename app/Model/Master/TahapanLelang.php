<?php

namespace App\Model\Master;

use App\Model\MyModel;
use Illuminate\Database\Eloquent\Model;
use App\Traits\Trackable;


class TahapanLelang extends MyModel
{
	use Trackable;

    protected $table = 'mst_tahapan_lelang';
    protected $primaryKey = 'id_tahapan_lelang';

    protected $fillable = ['urutan', 'nama_tahapan_lelang'];
    protected $hidden = ['id_tahapan_lelang'];
    protected $foreignKeys = [];
    protected $appends = ['id'];

}
