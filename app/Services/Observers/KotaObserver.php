<?php

namespace App\Services\Observers;

use App\Model\Master\User;
use App\Model\Master\Kota;
use Illuminate\Http\Request;

class KotaObserver{
	
	public function creating(Kota $kota){
		
		$request = request();
		$input = $request->header();

		if (isset($input['token'])){
			$auth = User::where('remember_token', $input['token'])->first();
	        $ip_address = $request->ip();
	        
			$kota->created_by = $auth->id;
			$kota->alamat_ip = $ip_address;
		}
	}

	public function updating(Kota $kota){

		$request = request();
		$input = $request->header();

		$auth = User::where('remember_token', $input['token'])->first();
        $ip_address = $request->ip();

        $kota->updated_by = $auth->id;
        $kota->alamat_ip = $ip_address;
	}

}