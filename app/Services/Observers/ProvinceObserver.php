<?php

namespace App\Services\Observers;

use App\Model\Master\User;
use App\Model\Master\Province;
use Illuminate\Http\Request;

class ProvinceObserver{
	
	public function creating(Province $kota){
		
		$request = request();
		$input = $request->header();

		if (isset($input['token'])){
			$auth = User::where('remember_token', $input['token'])->first();
	        $ip_address = $request->ip();
	        
			$kota->created_by = $auth->id;
			$kota->alamat_ip = $ip_address;
		}
	}

	public function updating(Province $kota){

		$request = request();
		$input = $request->header();

		$auth = User::where('remember_token', $input['token'])->first();
        $ip_address = $request->ip();

        $kota->updated_by = $auth->id;
        $kota->alamat_ip = $ip_address;
	}
}