<?php

namespace App\Services\Observers;

use App\Model\Master\User;
use Illuminate\Http\Request;

class UserObserver{
	
	public function creating(User $user){
		
		$request = request();
		$input = $request->header();
		
		if (isset($input['token'])){
			$auth = User::where('remember_token', $input['token'])->first();
	        $ip_address = $request->ip();
	        
			$user->created_by = $auth->id;
			$user->ip_address = $ip_address;
		}
	}

	public function updating(User $user){

		$request = request();
		$input = $request->header();

		$auth = User::where('remember_token', $input['token'])->first();
        $ip_address = $request->ip();

        $user->updated_by = $auth->id;
        $user->ip_address = $ip_address;
	}
	
}