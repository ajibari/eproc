<?php

namespace App\Services\Observers;

use App\Model\Master\User;
use App\Model\Master\Kecamatan;
use Illuminate\Http\Request;

class KecamatanObserver{
	
	public function creating(Kecamatan $user){
		
		$request = request();
		$input = $request->header();
		
		if (isset($input['token'])){
			$auth = User::where('remember_token', $input['token'])->first();
	        $ip_address = $request->ip();
	        
			$user->created_by = $auth->id;
			$user->alamat_ip = $ip_address;
		}
	}

	public function updating(Kecamatan $user){

		$request = request();
		$input = $request->header();

		$auth = User::where('remember_token', $input['token'])->first();
        $ip_address = $request->ip();

        $user->updated_by = $auth->id;
        $user->alamat_ip = $ip_address;
	}
	
}