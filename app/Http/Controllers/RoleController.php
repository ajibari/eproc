<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Master\RoleUser;
use App\Model\Master\Role;
use App\Helpers\Integra;

class RoleController extends Controller
{
    //
    public function create(Request $request){

        $input = $request->input();
        
        $role = new Role;
        $role->name         = $input['name'];
        $role->display_name = $input['display_name']; // optional
        $role->description  = $input['description']; // optional
        
        if($role->save()){
            $message = "Data success added.";
            return response()->json(['success'=> true, 'message'=> $message]);
        }

    }

    public function del(){
        $input = $request->input();

        $role = App\Role::find($id);
        
        if($role->history()->forceDelete()){
            $message = array('message' => 'sukses');
            echo json_encode($message);
        }
    }

    public function update(Request $request){
        
        $input = $request->input();
        $role = App\Role::find($id);
        
        $role->display_name = $input['display_name'];
        $role->description = $input['description'];
        
        $role->save();
    }

    public function del_soft(){
        $input = $request->input();

        App\Role::find($id)->delete();
    }

    public function get(){

    }

    public function get_by(){
        
    }

    public function tes_helper_autoload(){
        echo App\Helpers\Integra::fooBar();
    }
}
