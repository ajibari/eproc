<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Master\User;
use App\Model\Master\RoleUser;
use App\Model\UserVerification;
use App\Model\Log;
use Laratrust\Traits\LaratrustUserTrait;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Hashids;
use Validator;

class UserController extends Controller
{
    
    public $hashids;

    public function __construct(){
        //using hashid to encode and decode ID
        //salt key must be same with front end
        $salt = env("SALT_KEY");
        return $this->hashids = new Hashids\Hashids($salt);
    }

    public function create(Request $request){
        
        $form = $request->input();

        //validation
            $rules = [
                'name' => 'required|max:255',
                'email' => 'required|email|max:255|unique:users',
                'password' => 'required|confirmed|min:5'
            ];

            $input = $request->only(
                'name',
                'email',
                'password',
                'password_confirmation'
            );

            $validator = Validator::make($input, $rules);

            if($validator->fails()){
                $error = $validator->messages()->toJson();
                return renderResponse($input, false, $error);

            }

        //save to users and return id
            $user = User::create(
                ['name' => $input['name'], 'email' => $input['email'], 'password' => Hash::make($input['password']), 'is_verified' => 0]
            );

            $error = env('RESPONSE_SAVE_FAILED');

            if(!$user){
                return renderResponse($input, false, $error);
            }

        //save to user_verifications 
            $verification_code = str_random(30);
            $user_verification = UserVerification::insert(
                ['user_id' => $user->id, 'token' => $verification_code]
            );

            if(!$user_verification){
                return renderResponse($input, false, $error);
            }

        //response 
            $success = env('RESPONSE_USER_ADD_SUCCESS');

            return renderResponse($input, true, $success);

    }
    
    public function createBulk($arr, $userVerify = null){
        //validation
            $rules = [
                'name' => 'required|max:255',
                'email' => 'requ ired|email|max:255|unique:users',
                'password' => 'required|min:5'
            ];

            $db = env("DB_CONNECTION", "mysql");

            switch ($db) {
                case 'mysql':
                    $this->createAtMySQL($arr, $userVerify);
                    break;
                case 'sqlsrv':
                    $this->createAtSQLServer($arr, $userVerify);
                    break;
                case 'pgsql':
                    $this->createAtPostgreSQL($arr, $userVerify);
                    break;
            }
    }

    public function createByCsv(){
        //extract CSV into Array then call createBulk()
        $data = array(
            array('name' => 'John', 'email' => 'john12@email.com', 'password' => 'password','role' => 3),
            array('name' => 'beti', 'email' => 'beti12@email.com', 'password' => 'password','role' => 3),
            array('name' => 'Layla', 'email' => 'Layla12@email.com', 'password' => 'password','role' => 3),
        );

        $this->createBulk($data, 1);
        return $data;
    }

    public function createByExcel($fileLoc){
  
    }

    public function get($hashed_id){

        $id = $this->hashids->decode($hashed_id);

        $error = env('RESPONSE_NO_DATA');

        if(!isset($id[0])){
            return renderResponse($id, false, $error);
        }
    
        $result = User::find($id[0]);

        if($result->isEmpty()){
            return renderResponse($id, false, $error);
        }

        $success = env('RESPONSE_GET_DATA');
        return renderResponse($result, true, $success);

    }

    public function get_by(Request $request, $param){

        $form = $request->input();
        $error = env('RESPONSE_NO_DATA');

        if($param == 'id'){
            $data_decode = $this->hashids->decode($form['value']);
            
            if(!isset($data_decode[0])){
                return renderResponse($param.'='.$form['value'], false, $error);
            }

            $value = $data_decode[0];
        }

        $result = User::where($param, 'like', '%'.$form['value'].'%')->get();

        if($result->isEmpty()){
            return renderResponse($param.' = '.$form['value'], false, $error);
        }

        $success = env('RESPONSE_GET_DATA');
        return renderResponse($result, true, $success);

    }

    public function get_all(){
        
        $result = User::get();
        $message = env('RESPONSE_GET_SUCCESS');

        if(!isset($result[0]->id)){
            $message = env('RESPONSE_GET_FAILED');
            $result = null;
        }
        
        $message = env('RESPONSE_GET_DATA');
        return renderResponse($result, true, $message);

    }

    public function get_token(){

        $result = User::select('id','remember_token')->whereNotNull('remember_token')->get();
        $result = $result->makeVisible(['remember_token']);
        
        $message = env('RESPONSE_GET_SUCCESS');

        if($result->isEmpty()){
            $message = env('RESPONSE_GET_SUCCESS');
            $result = null;
        }
    
        $response = ['Response' => $result, 'Success' => true, 'Message' => $message];

        return response()->json($response);
    }

    public function del(Request $request, $hashed_id){

        $id = $this->hashids->decode($hashed_id);
        $error = env('RESPONSE_NO_DATA');

        if(!isset($id[0])){
            return renderResponse($hashed_id, false, $error);
        }

        $user = User::find($id[0]);

        if (!$user->delete($id)) {

            $error = env("RESPONSE_DEL_FAILED");
            return renderResponse($hashed_id, true, $error);

        }

        $success = env("RESPONSE_DEL_SUCCESS");
        return renderResponse($hashed_id, true, $success);

    }

    public function del_bulk(Request $request){
        $input = $request->input();
        $user = User::whereIn('id', $input['id']);
          
        if ($user->delete()) {
          $response['success'] = true;
          $response['result'] = 'Success delete users!';
          echo response()->json($response);
        }
    }

    public function update(Request $request){
        
        $input = $request->input();
        $data = $input;
        
        $error = env('RESPONSE_UPDATE_FAILED');

        $id = $this->hashids->decode($input['id']);
        
        if(!isset($id[0])){
            return renderResponse($input, false, $error);
        }

        unset($data['id']);
        
        $user = User::find($id[0]);

        if(!$user){
            return renderResponse($input, false, $error);
        }

        $update = $user->update($data);

        if(!$update){
            return renderResponse($input, false, $error);
        }

        $success = env('RESPONSE_UPDATE_SUCCESS');
        return renderResponse($input, true, $success);
    }

    private function createAtMySQL($arr, $userVerify){

        $is_verified = 0;
        if(!is_null($userVerify)){
            $is_verified = $userVerify;
        }

        $i = 1;
        $sql = '';
        $now = date('Y-m-d H:i:s');


        DB::beginTransaction();

            if(!empty($arr)){

                //query pertama make sure auto_increment max_id + 1
                    $sql0 = "ALTER TABLE `users` AUTO_INCREMENT = 1;";
                    DB::statement($sql0);
                
                //query kedua insert ke users
                    $sql1 = "insert into users (name, email, password, is_verified, created_at) values ";
                    $id_max = User::max('id');

                    foreach ($arr as $value) {
                        $sql1 .= '("'.$value['name'].'","'.$value['email'].'","'.Hash::make($value['password']).'","'.$is_verified.'","'.$now.'"), ';
                        $id_increment[] = ++$id_max;
                    }

                    session(['table_user' => $id_increment]);
                    $sql1 = rtrim($sql1,", ");
                    $sql1 .= ";";

                    DB::statement($sql1);

                //query ketiga attach role ke users
                    $sql2 = "insert into role_user (role_id, user_id, user_type) values ";
                    $x = 0;

                    foreach ($arr as $value) {
                        $sql2 .= '('.$value['role'].','.$id_increment[$x].',"App\\\User"), ';
                        $x++;
                    }

                    $sql2 = rtrim($sql2,", ");
                    $sql2 .= ";";
                    DB::statement($sql2);

                //query keempat untuk verifikasi
                    if($is_verified == 1){
                        $sql3 = "insert into user_verifications (user_id, token, created_at) values ";
                        
                        $x = 0;
                        foreach ($arr as $value) {
                            $token = str_random(30);
                            $sql3 .= '('.$id_increment[$x].',"'.$token.'","'.$now.'"), ';
                            $x++;
                        }       

                        $sql3 = rtrim($sql3,", ");
                        $sql3 .= ";";
                        DB::statement($sql3);                 
                    }
            }

        DB::commit();
        exit;

    }

    private function createAtSQLServer($arr){

    }

    private function createAtPostgreSQL($arr){
        //kayaknya script nya sama dengan yang di createAtMySQL();
    }

    public function hashed($id){
        return $this->encode($id);
    }

    public function unhashed($hashed_id){ 
        return $this->decode($hashed_id);
    }

    public function coba(){
        $cond = ['bugName' => 'KecoakX'];
        $dbMongo = 'my_database';
        $collection = 'bugs';
        echo getMongo($cond, $dbMongo, $collection);
    }



























    

    public function error_response(){
        $message = array('message' => 'You don\'t have authorization');
        echo response()->json($message);
    }
    public function profile(Request $request){
        dd($request->input('data'));
        echo 'tes profile';
        exit;
        $user = User::where('name', '=', 'bari')->first();
        // $user->attachRole('user');
        if($user->hasRole('user')){
            echo json_encode(array('content' => 'profile'));
            exit;
        }
    }
    public function read_eloquent(){
        header('Content-Type: application/json');
        echo RoleUser::find(2)->permission()->get()->toJson();
        exit;
        // echo RoleUser::find(2)->permission()->get()->toJson();
        // exit;
        echo response()->json(RoleUser::where('role_id',2)->get());
        exit;
        $data = User::find(1)->role()->get()->toJson();
        echo ($data);
    }

    public function get_info(Request $request){

    }

    public function log(){
        $todo = new Log();
        $users = $todo->cari();
        dd($users);
    }

    public function upload(Request $request){
        $input = $request->input();
        $file = $request->file();

        $file = 
        // dd($file);
        
        // dd($input);

        //Move Uploaded File
          $destinationPath = 'uploads';
          $request->file_upload->store('images');
          // $file->move( $destinationPath, $file->getClientOriginalName() );
    }

    public function form_upload(){
        return view('form_upload');
    }
}
