<?php

namespace App\Http\Controllers\Tender;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Lelang\Lelang;
use App\Traits\Trackable;
use Validator;

class TenderController extends Controller
{   
    use Trackable;

    public function getActiveLelang(){
        $error = env('RESPONSE_NO_DATA');
        $result = lelang::with(['pengadaan','organisasi_perusahaan','mata_uang','jadwal_tahapan_lelang'])->get();

        if(!$result){
            return renderResponse($hashed_id, false, $error);
        }

        $success = env('RESPONSE_GET_DATA');
        return renderResponse($result, true, $success);
    }

    public function getActiveLelangByOrg(){

    }

    public function getActiveLelangLimit(){

    }

    public function getActiveLelangByOrgLimit(){

    }

    public function getLelangById($id_lelang){

    }

    public function createLelang(Request $request){
        $input = $request->input();

        //validation
            $rules = [
                'id_pengadaan' => 'required',
                'id_organisasi_perusahaan' => 'required',
                'nama_paket_lelang' => 'required',
                'jenis_lelang' => 'required',
                'kualifikasi_grade' => 'required',
                'grade' => 'required',
                'id_mata_uang' => 'required',
                'persentase_batas_kewajaran' => 'required',
                'pra_kualifikasi' => 'required|boolean',
                'perlu_aanwijzing' => 'required|boolean',
                'perlu_eauction' => 'required|boolean',
                'status_submit' => 'nullable|boolean',
                'status_approve' => 'nullable|boolean',
                'approved_by' => 'nullable',
                'tanggal_approve' => 'nullable|date',
                'status_broadcast' => 'nullable|boolean'
            ];

            $validator = Validator::make($input, $rules);

            if($validator->fails()){
                $error = $validator->messages()->toJson();
                return renderResponse($input, false, $error);
            }

        //daftar input yang harus di decrypt
            $decryted_list = [
                'id_pengadaan', 
                'id_organisasi_perusahaan',
                'id_mata_uang',
                'approved_by'
            ];

            $data_input = $this->decryptList($input, $decryted_list);
            
        Lelang::create($data_input);
        $message = env('RESPONSE_SAVE_SUCCESS');
        return renderResponse($input, true, $message);
    }

    public function delLelang(Request $request){
        $model = new Lelang();

        $input = $request->input();
        $primaryKey = $model->getKeyName();
        $id = $this->decode($input[$primaryKey]);
        
        //search
            $lelang = Lelang::find($id);

        //delete
            $del = $lelang->delCheckRelationship($lelang);

            if($del !== true){
                return renderResponse($input, false, $del);
            }

            $message = env('RESPONSE_DEL_SUCCESS');
            return renderResponse($input, true, $message);
    }

    public function approveLelang(Request $request){

    }

    public function updateLelang(Request $request){

        $input = $request->input();
        //validation
            $rules = [
                'id_lelang' => 'required',
                'id_pengadaan' => 'required',
                'id_organisasi_perusahaan' => 'required',
                'nama_paket_lelang' => 'required',
                'jenis_lelang' => 'required',
                'kualifikasi_grade' => 'required',
                'grade' => 'required',
                'id_mata_uang' => 'required',
                'persentase_batas_kewajaran' => 'required',
                'pra_kualifikasi' => 'required|boolean',
                'perlu_aanwijzing' => 'required|boolean',
                'perlu_eauction' => 'required|boolean',
                'status_submit' => 'nullable|boolean',
                'status_approve' => 'nullable|boolean',
                'approved_by' => 'nullable',
                'tanggal_approve' => 'nullable|date',
                'status_broadcast' => 'nullable|boolean'
            ];

            $validator = Validator::make($input, $rules);

            if($validator->fails()){
                $error = $validator->messages()->toJson();
                return renderResponse($input, false, $error);
            }

        //daftar input yang harus di decrypt
            $decryted_list = [
                'id_lelang',
                'id_pengadaan', 
                'id_organisasi_perusahaan',
                'id_mata_uang',
                'approved_by'
            ];

            $data_input = $this->decryptList($input, $decryted_list);
        
        $model = new Lelang();
        $primaryKey = $model->getKeyName();
        $Lelang = Lelang::find($data_input[$primaryKey]);

        foreach($data_input as $k => $v){
            $Lelang->$k = $data_input[$k];
        }

        $save = $Lelang->save();

        if(!$save){
            $message = env('RESPONSE_UPDATE_FAILED');
            return renderResponse($data['input'], false, $message);
        }

        $message = env('RESPONSE_UPDATE_SUCCESS');
        return renderResponse($input, true, $message);
    }

    public function submitLelang(Request $request){
        //update data lelang status_submit = true
        //dan lock update data lelang
        $input = $request->input();

        //validation
            $rules = [
                'id_lelang' => 'required'
            ];

            $validator = Validator::make($input, $rules);

            if($validator->fails()){
                $error = $validator->messages()->toJson();
                return renderResponse($input, false, $error);
            }

        //daftar input yang harus di decrypt
            $decryted_list = [
                'id_lelang'
            ];

            $data_input = $this->decryptList($input, $decryted_list);
        
        $model = new Lelang();
        $primaryKey = $model->getKeyName();
        $Lelang = Lelang::find($data_input[$primaryKey]);

        $Lelang->status_submit = true;
        $save = $Lelang->save();

        if(!$save){
            $message = env('RESPONSE_UPDATE_FAILED');
            return renderResponse($data['input'], false, $message);
        }

        $message = env('RESPONSE_UPDATE_SUCCESS');
        return renderResponse($input, true, $message);
    }

}
