<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Faker;

class FakerController extends Controller
{
    public function generate_dummy(){
        
        $faker = Faker\Factory::create();
    	$limit = 23000;	
    	$data = [];

        for ($i = 0; $i < $limit; $i++) {

            // DB::table('faker')->insert([ //,
            //     'name' => $faker->name,
            //     'email' => $faker->unique()->email,
            //     'contact_number' => $faker->phoneNumber,
            // ]);

            $data[$i] = ['name' => $faker->name, 'email' => $faker->email, 'contact_number' => $faker->phoneNumber];
        }

        return response()->json($data);
    }
}
