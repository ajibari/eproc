<?php

namespace App\Http\Controllers\Vendor;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use App\Exception\Handler;
use Illuminate\Support\Facades\DB;
use App\Model\Vendor\Rekanan;
use App\Model\Vendor\RekananLokal;
use App\Model\Vendor\RekananBidangUsaha;
use App\Model\Vendor\RekananPic;
use App\Model\Vendor\RekananDokumen;
use App\Model\Vendor\RekananOrganisation;
use App\Model\Vendor\RekananApproval;
use App\Model\Master\GlobalSetting;
use App\Model\Master\GlobalSettingConfig;
use App\Model\Master\OrganisasiPerusahaan;
use Illuminate\Support\Arr;

class VendorController extends Controller
{
    public function insertLocal(Request $request){
        //registrasi vendor lokal
        //insert into rekanan, rekanan_lokal, rekanan_bidang_usaha, rekanan_dokumen, rekanan_pic
        //insert into rekanan_approval
        
        $input = $request->input();

        //validation
            $rules = [
                //rekanan
                    'perusahaan_lokal' => 'required',
                    'kode_rekanan' => 'required',
                //rekanan_lokal
                    'no_npwp' => 'required',
                    'nama_perusahaan' => 'required', 
                    'id_tipe_perusahaan' => 'required', 
                    'id_jenis_perusahaan' => 'required', 
                    'id_provinsi' => 'required', 
                    'id_kota' => 'required',
                    'id_kecamatan' => 'required', 
                    'kode_pos' => 'required',
                    'detail_alamat' => 'required',
                    'id_cakupan_wilayah' => 'required', 
                    'id_kualifikasi_perusahaan' => 'required', 
                    'email' => 'required', 
                    'telepon' => 'required', 
                    'fax' => 'required',
                    'website' => 'required',
                //rekanan_bidang_usaha
                    'id_bidang_usaha' => 'required',
                    'id_bidang_usaha_detail' => 'required',
                //rekanan_pic
                    'nama_depan' => 'required', 
                    'nama_belakang' => 'required', 
                    'jabatan' => 'required', 
                    'email' => 'required', 
                    'telepon' => 'required', 
                    'telepon_ext' => 'required', 
                    'mobile_phone' => 'required',
                    'alamat' => 'required',
                //rekanan_dokumen
                    'id_dokumen_perusahaan' => 'required', 
                    'tanggal_mulai' => 'required', 
                    'tanggal_selesai' => 'required', 
                    'file_path' => 'required', 
                    'keterangan'  => 'required',
                //rekanan_org
                    'id_organisasi_perusahaan' => 'required'
            ];

            $validator = Validator::make($input, $rules);

            if($validator->fails()){
                $error = $validator->messages()->toJson();
                return renderResponse($input, false, $error);
            }
        
        //load required model
            $model = new Rekanan();
            $primaryKey = $model->getKeyName();
            $foreignKeys = $model->getForeignKeys();
            $fillable_keys = is_null($model->getFillable())?[]:$model->getFillable();
            
            $model2 = new RekananLokal();
            $primaryKey2 = $model2->getKeyName();
            $foreignKeys2 = $model2->getForeignKeys();

            $model3 = new RekananBidangUsaha();
            $primaryKey3 = $model3->getKeyName();
            $foreignKeys3 = $model3->getForeignKeys();

            $model4 = new RekananPic();
            $primaryKey4 = $model4->getKeyName();
            $foreignKeys4 = $model4->getForeignKeys();

            $model5 = new RekananDokumen();
            $primaryKey5 = $model5->getKeyName();
            $foreignKeys5 = $model5->getForeignKeys();

            $model6 = new RekananOrganisation();
            $primaryKey6 = $model6->getKeyName();
            $foreignKeys6 = $model6->getForeignKeys();

        //get all foreign keys (unhashed ID)
            $FK = array_merge($foreignKeys, $foreignKeys2);
            $FK = array_merge($FK, $foreignKeys3);
            $FK = array_merge($FK, $foreignKeys4);
            $FK = array_merge($FK, $foreignKeys5);
            $FK = array_merge($FK, $foreignKeys6);

            $FK = array_values(array_unique($FK));
        
        //cek kalau ada input yang di hash
            $input_processed = $input;
            foreach ($input as $key => $value) {
                //jika id hashed merupakan array / multi value
                    if(in_array($key, $FK)){
                        if(is_array($value)){
                            $i = 0;
                            foreach($value as $k => $v){
                                $decode = $this->decode($v);
                                $input_processed[$key][] = $decode;
                                unset($input_processed[$key][$i]);
                                $i++;
                            }
                            
                            $input_processed[$key] = array_values($input_processed[$key]);
                        }
                        else{
                            $decode = $this->decode($value);
                            $input_processed[$key] = $decode;
                        }
                    }
            }

        //ambil input yang perlu aja untuk masing-masing table
            $field_rekanan = ['perusahaan_lokal', 'kode_rekanan'];
            $field_rekanan_lokal = [
                'no_npwp',
                'nama_perusahaan',
                'id_tipe_perusahaan',
                'id_jenis_perusahaan',
                'id_provinsi',
                'id_kota',
                'id_kecamatan',
                'kode_pos',
                'detail_alamat',
                'id_cakupan_wilayah',
                'id_kualifikasi_perusahaan',
                'email',
                'telepon',
                'fax',
                'website'
            ];
            $field_rekanan_bidang_usaha = ['id_bidang_usaha', 'id_bidang_usaha_detail'];
            $field_rekanan_pic = ['nama_depan', 'nama_belakang', 'jabatan', 'email', 'telepon', 'telepon_ext', 'mobile_phone', 'alamat'];
            $field_rekanan_dokumen = ['id_dokumen_perusahaan', 'tanggal_mulai', 'tanggal_selesai', 'file_path', 'keterangan'];
            $field_rekanan_org = ['id_organisasi_perusahaan'];

        //input yang dibutuhkan rekanan
            $input_rekanan = Arr::only($input_processed, $field_rekanan); 
            $save = Rekanan::create($input_rekanan);
            
            $id_raw = $save->id;
            $id_rekanan = $this->decode($id_raw[$primaryKey]);

            $data = $model::where($primaryKey, $id_rekanan)->first();

        //input yang dibutuhkan rekanan_lokal
            $input_rekanan_lokal = Arr::only($input_processed, $field_rekanan_lokal);
            $rekanan_lokal = $data->rekanan_lokal()->create($input_rekanan_lokal);

        //input yang dibutuhkan rekanan_bidang_usaha
            $input_rekanan_bidang_usaha = Arr::only($input_processed, $field_rekanan_bidang_usaha);
            $rekanan_bidang_usaha = $data->rekanan_bidang_usaha()->create($input_rekanan_bidang_usaha);

        //input yang dibutuhkan rekanan_pic
            $input_rekanan_pic = Arr::only($input_processed, $field_rekanan_pic);
            $rekanan_pic = $data->rekanan_pic()->create($input_rekanan_pic);

        //input yang dibutuhkan rekanan_dokumen
            $input_rekanan_dokumen = Arr::only($input_processed, $field_rekanan_dokumen);
            $rekanan_dokumen = $data->rekanan_dokumen()->create($input_rekanan_dokumen);

        //input yang dibutuhkan rekanan_org
            $input_rekanan_org = Arr::only($input_processed, $field_rekanan_org);
            $key = key($input_rekanan_org);
            if(is_array($input_rekanan_org)){
                foreach($input_rekanan_org['id_organisasi_perusahaan'] as $iro => $v_iro){
                    $input_v_iro[$key] = $v_iro;
                    $rekanan_org = $data->rekanan_org()->create($input_v_iro);
                    unset($input_v_iro);
                }
            }

        //proses untuk listing approval
            $listing_approval = $this->createApproval($id_rekanan);

        if(!$listing_approval){
            $message = env('RESPONSE_SAVE_FAILED');
            return renderResponse($input, false, $message);            
        }

        $message = env('RESPONSE_SAVE_SUCCESS');
        return renderResponse($input, true, $message);
    }

    public function insertAsing(){
        //registrasi vendor asing
        //insert into rekanan, rekanan_asing, rekanan_bidang_usaha, rekanan_dokumen, rekanan_pic
        //insert into rekanan_approval
    }

    public function createApproval($id_rekanan){
        //read "approval_calon_vendor" di table mst_global_setting
        $var = env('GLOBAL_SETTING_APPROVAL_VENDOR');

        //sort berdasarkan sequence
        $setting = GlobalSetting::with(['config' => function($query){
            $query->orderBy('sequence', 'asc');
        }])->where('nama_parameter', $var)->first()->toArray();

        if(!empty($setting['nilai_parameter'])){
            
            for ($i=0; $i < $setting['nilai_parameter']; $i++) { 

                //read config approval (mst_global_setting)
                    $id_global_setting_config_hash = $this->decode($setting['config'][$i]['id']['id_global_setting_config']);
                    $id_global_setting_config = $id_global_setting_config_hash;

                //save data to rekanan_approval    
                    $data = ['id_rekanan' => $id_rekanan, 'keterangan' => 'waiting approval', 'status' => null, 'id_global_setting_config' => $id_global_setting_config];
                   
                    RekananApproval::create($data);
            }

            return true;
        }

        return false;
    }

    public function getNeedApproval(){
        $collection = Rekanan::with(['rekanan_lokal', 'rekanan_approval' => function($query) {
            $query->whereNull('status');
        }])->has('rekanan_approval')->get()->toArray();

        $i = 0;
        foreach($collection as $c => $v){
            if(empty($v['rekanan_approval'])){
                unset($collection[$i]);
            }
            $i++;
        }

        $result = array_values($collection);

        $message = env('RESPONSE_GET_SUCCESS');
        return renderResponse($result, true, $message);
    }

    public function getAllStatusApproval(){
        $rekanan_approval = Rekanan::with('rekanan_approval')->has('rekanan_approval')->get();

        $message = env('RESPONSE_GET_SUCCESS');
        return renderResponse($rekanan_approval, true, $message);
    }

    public function getAllAcceptedApproval(){
        $rekanan_approval = Rekanan::with(['rekanan_approval' => function($query){
            $query->where('status',1);
        }])->has('rekanan_approval')->get();   

        //jika belum ada vendor yang di approve
            if(empty($rekanan_approval->rekanan_approval)){
                $message = env('RESPONSE_GET_FAILED');
                return renderResponse('', false, $message);    
            }

        $message = env('RESPONSE_GET_SUCCESS');
        return renderResponse($rekanan_approval, true, $message);
    }

    public function getActiveVendor(){
        $collection = Rekanan::with(['rekanan_lokal', 'rekanan_approval' => function($query){
            $query->where('status', true);
        }])->get()->toArray();

        //manipulasi array
            $i = 0;
            foreach($collection as $c => $v){
                if(empty($v['rekanan_approval'])){
                    unset($collection[$i]);
                }
                $i++;
            }

            $result = array_values($collection);

        if(empty($result)){
            $message = env('RESPONSE_GET_FAILED');
            return renderResponse('', false, $message);
        }

        $message = env('RESPONSE_GET_SUCCESS');
        return renderResponse($result, true, $message);
    }

    public function getRejectedVendorLokal(){
        //where rekanan_approval.status = 0
        $collection = Rekanan::with(['rekanan_lokal', 'rekanan_approval' => function($query){
            $query->where('status', false);
        }])->get()->toArray();

        //manipulasi array
            $i = 0;
            foreach($collection as $c => $v){
                if(empty($v['rekanan_approval'])){
                    unset($collection[$i]);
                }
                $i++;
            }

            $result = array_values($collection);

        if(empty($result)){
            $message = env('RESPONSE_GET_FAILED');
            return renderResponse('', false, $message);
        }

        $message = env('RESPONSE_GET_SUCCESS');
        return renderResponse($result, true, $message);
    }

    public function getStatusByVendor($id_vendor){
        //fungsi untuk mendapatkan status approval vendor by id (internal purpose)
            $id_rekanan = $this->decode($id_vendor);

            if(!isset($id_rekanan)){
                $error = env('RESPONSE_NO_DATA');
                return renderResponse($id_vendor, false, $error);
            }

            $model = new RekananApproval();
            $primaryKey = $model->getKeyName();
            $foreignKeys = is_null($model->getForeignKeys())?[]:$model->getForeignKeys();
            $fillable_keys = is_null($model->getFillable())?[]:$model->getFillable();

            array_push($foreignKeys, $primaryKey);
            $hashed = $foreignKeys;
            
            $rekanan_approval = Rekanan::with(['rekanan_lokal','rekanan_approval' => function($query){
                $query->orderBy('id_rekanan_approval', 'asc');
            }, 'rekanan_approval.global_setting_config'])->where('id_rekanan', $id_rekanan)->has('rekanan_approval')->get();
            // dd($rekanan_approval);
            
            if($rekanan_approval->isEmpty()){
                $message = env('RESPONSE_GET_FAILED');
                return renderResponse($id_vendor, false, $message);
            }

            $approval_count = $rekanan_approval[0]->rekanan_approval->count();

            for($i = 0; $i < $approval_count; $i++){
                $approval_status[] = [
                    'id_organisasi_perusahaan' => $rekanan_approval[0]->rekanan_approval[$i]->global_setting_config->id_value, 
                    'status' => $rekanan_approval[0]->rekanan_approval[$i]->global_setting_config->status
                ];
            }

            return $approval_status;
    }

    public function getObjectDataByVendor($id_vendor){
        $id_rekanan = $this->decode($id_vendor);

        if(!isset($id_rekanan)){
            $error = env('RESPONSE_NO_DATA');
            return renderResponse($id_vendor, false, $error);
        }

        $model = new RekananApproval();
        $primaryKey = $model->getKeyName();
        $foreignKeys = is_null($model->getForeignKeys())?[]:$model->getForeignKeys();
        $fillable_keys = is_null($model->getFillable())?[]:$model->getFillable();

        array_push($foreignKeys, $primaryKey);
        $hashed = $foreignKeys;
        
        $rekanan_approval = Rekanan::with(['rekanan_lokal','rekanan_approval' => function($query){
            $query->orderBy('id_rekanan_approval', 'asc');
        }, 'rekanan_approval.global_setting_config'])->where('id_rekanan', $id_rekanan)->has('rekanan_approval')->get();
        
        if($rekanan_approval->isEmpty()){
            $message = env('RESPONSE_GET_FAILED');
            return renderResponse($id_vendor, false, $message);
        }
        
        $message = env('RESPONSE_GET_SUCCESS');
        return renderResponse($rekanan_approval, true, $message);
    }

    public function getVendorByOrg($id_org){
        $id_organisasi_perusahaan = $this->decode($id_org);

        $error = env('RESPONSE_NO_DATA');

        if(!isset($id_organisasi_perusahaan)){
            return renderResponse($id_org, false, $error);
        }   

        $result = RekananApproval::with(['rekanan_lokal','rekanan_org' => function($query) use($id_organisasi_perusahaan){
            $query->where('id_organisasi_perusahaan', '=', $id_organisasi_perusahaan);
        }])->has('rekanan_org')->get();

        if(!$result){
            return renderResponse($id_org, false, $error);
        }

        $success = env('RESPONSE_GET_DATA');
        return renderResponse($result, true, $success);
    }

    public function getVendorClassification($id_vendor){
        $id_rekanan = $this->decode($id_vendor);

        $error = env('RESPONSE_NO_DATA');

        if(!isset($id_rekanan)){
            return renderResponse($id_vendor, false, $error);
        }

        $result = Rekanan::with(['rekanan_bidang_usaha', 'rekanan_bidang_usaha.bidang_usaha_detail'])->where('id_rekanan', $id_rekanan)->get();
        
        if($result->isEmpty()){
            return renderResponse($id_vendor, false, $error);
        }

        $message = env('RESPONSE_GET_DATA');
        return renderResponse($result, true, $message);
    }

    public function getVendorDocuments($id_vendor){
        $id_rekanan = $this->decode($id_vendor);

        $error = env('RESPONSE_NO_DATA');

        if(!isset($id_rekanan)){
            return renderResponse($id_vendor, false, $error);
        }

        $result = Rekanan::with(['rekanan_dokumen', 'rekanan_dokumen.dokumen_perusahaan'])->where('id_rekanan', $id_rekanan)->get();
        
        if($result->isEmpty()){
            return renderResponse($id_vendor, false, $error);
        }

        $message = env('RESPONSE_GET_DATA');
        return renderResponse($result, true, $message);
    }

    public function approveVendor(Request $request){
        
        $input = $request->input();
        $parameter = env('GLOBAL_SETTING_APPROVAL_VENDOR');
        $sequence = env('GLOBAL_SETTING_APPROVAL_SEQUENCE');
        
        $OrganisasiPerusahaan = new OrganisasiPerusahaan();

        $model = new RekananApproval();
        $primaryKey = $model->getKeyName();
        $foreignKeys = is_null($model->getForeignKeys())?[]:$model->getForeignKeys();
        $fillable_keys = is_null($model->getFillable())?[]:$model->getFillable();

        array_push($foreignKeys, $primaryKey);
        $hashed = $foreignKeys;
        
        //cek kalau ada input yang di hash 
            $input_processed = $input;
            foreach ($input as $key => $value) {

                //jika id hashed merupakan array / multi value
                    if(in_array($key, $hashed)){
                        if(is_array($value)){
                            $i = 0;
                            foreach($value as $k => $v){
                                $decode = $this->decode($v);
                                $input_processed[$key][] = $decode;
                                unset($input_processed[$key][$i]);
                                $i++;
                            }
                            
                            $input_processed[$key] = array_values($input_processed[$key]);
                        }
                        else{
                            $decode = $this->decode($value);
                            $input_processed[$key] = $decode;
                        }
                    }
            }

        $id_rekanan = $input_processed['id_rekanan'];
        $rekanan_approval = Rekanan::with(['rekanan_approval' => function($query){
            //sort berdasarkan id_rekanan_approval karna data yang tersimpan sudah berdasarkan sequence
                $query->orderBy('id_rekanan_approval', 'asc');
        }, 'rekanan_approval.global_setting_config'])->where('id_rekanan', $id_rekanan)->has('rekanan_approval')->get();

        $approval_count = $rekanan_approval[0]->rekanan_approval->count();

        for($i = 0; $i < $approval_count; $i++){
            $approval_status[] = [
                'id_organisasi_perusahaan' => $rekanan_approval[0]->rekanan_approval[$i]->global_setting_config->id_value, 
                'id_global_setting_config' => $rekanan_approval[0]->rekanan_approval[$i]->id_global_setting_config,
                'status' => $rekanan_approval[0]->rekanan_approval[$i]->global_setting_config->status
            ];
        }
        
        //cek current user info
            $token = $request->header('token');
            $user = getUserInfo($token);
            $user_organization = $user->id_organisasi_perusahaan;

            if(empty($user->id_organisasi_perusahaan) || is_null($user->id_organisasi_perusahaan)){
                $message = env('APPROVAL_VENDOR_FAILED');
                return renderResponse($input, false, $message);
            }

        //cek config sequence
            $approval_sequence = env('GLOBAL_SETTING_APPROVAL_SEQUENCE');

            //jika sequence approval di config = TRUE, maka vendor dapat diproses approve jika approve sequence sebelumnya sudah didapatkan

                if($approval_sequence){

                    $i = 0;

                    foreach($approval_status as $app => $value){
                        if($value['id_organisasi_perusahaan'] == $user_organization && $value['status'] != 1){
                            //cek sequence approval sebelumnya
                            
                            if(array_search($user_organization, $value)){
                                
                                //jika current user bukan approval pertama
                                    if($i > 0){

                                        //pengecekan status approval sebelumnya
                                            if($approval_status[$i-1]['status'] != true){
                                                //butuh approval sebelumnya
                                                    $message = env('APPROVAL_VENDOR_NEED_PREVIOUS_APPROVAL');
                                                    return renderResponse($input, false, $message);
                                            }
                                            else{
                                                //update rekanan approvalnya
                                                    $data = ['keterangan' => $input['keterangan'], 'status' => 1];
                                                    $RekananApproval = RekananApproval::where('id_rekanan', $id_rekanan)->where('id_global_setting_config', $approval_status[$i]['id_global_setting_config'])->update($data);

                                                    if(!$RekananApproval){
                                                        $message = env('RESPONSE_UPDATE_FAILED');
                                                        return renderResponse($input, false, $message);
                                                    }

                                                    $message = env('RESPONSE_UPDATE_SUCCESS');
                                                    return renderResponse($input, true, $message);
                                            }

                                    }

                                //langsung update approval, karena merupakan sequence pertama approver
                                    else{
                                        $data = ['keterangan' => $input['keterangan'], 'status' => 1];
                                        $RekananApproval = RekananApproval::where('id_rekanan', $id_rekanan)->where('id_global_setting_config', $approval_status[$i]['id_global_setting_config'])->update($data);

                                        if(!$RekananApproval){
                                            $message = env('RESPONSE_UPDATE_FAILED');
                                            return renderResponse($input, false, $message);
                                        }

                                        $message = env('RESPONSE_UPDATE_SUCCESS');
                                        return renderResponse($input, true, $message);
                                    }

                            }
                        }
                        else if($value['id_organisasi_perusahaan'] == $user_organization && $value['status'] == 1){
                            //sudah di approve sebelumnya
                            $message = env('APPROVAL_VENDOR_APPROVED');
                            return renderResponse($input, false, $message);
                        }

                        $i++;
                    }
                }

            //jika sequence approval di config = FALSE / NULL, tidak pengaruh ke sequence
                else{

                        $i = 0;

                        foreach($approval_status as $app => $value){
                            if(array_search($user_organization, $value)){
                                //update rekanan approvalnya
                                    $data = ['keterangan' => $input['keterangan'], 'status' => 1];
                                    $RekananApproval = RekananApproval::where('id_rekanan', $id_rekanan)->where('id_global_setting_config', $approval_status[$i]['id_global_setting_config'])->update($data);

                                    if(!$RekananApproval){
                                        $message = env('RESPONSE_UPDATE_FAILED');
                                        return renderResponse($input, false, $message);
                                    }

                                    $message = env('RESPONSE_UPDATE_SUCCESS');
                                    return renderResponse($input, true, $message);
                            }
                        }

                        
                }


                exit;




        //get list approver
            $config = GlobalSetting::with(['config' => function($query){
                $query->orderBy('sequence', 'asc');
            },'config.organization'])->where('nama_parameter', $parameter)->get();
            $config_result = $config->toArray();

            $org_count = $config[0]->config->count();

            for($i = 0; $i < $org_count; $i++){
                $organization[] = $config[0]->config[$i]->organization->id_organisasi_perusahaan;
            }
            
        

        //match $user with $config (organization)
            $approval_sequence = env('GLOBAL_SETTING_APPROVAL_SEQUENCE');
            $id_rekanan = $this->decode($input['id_rekanan']);

            if($approval_sequence){
                //jika sequence approval di config = TRUE
                
            }

            else{
                //jika sequence approval di config = FALSE / NULL
            }
        
        return renderResponse($config, true, '');
    }

    public function rejectVendor($id_vendor){

    }

    public function updateClassification(Request $request){
        $input = $request->input();
        
        //validation
            $rules = [
                'id_rekanan' => 'required',
                'id_rekanan_bidang_usaha' => 'required',
                'id_bidang_usaha' => 'required',
                'id_bidang_usaha_detail' => 'required',
                'keterangan' => 'required'
            ];

            $validator = Validator::make($input, $rules);

            if($validator->fails()){
                $error = $validator->messages()->toJson();
                return renderResponse($input, false, $error);
            }

        $model = new RekananBidangUsaha();
        $foreignKeys = $model->getForeignKeys();
        $primaryKey = $model->getKeyName();

        array_push($foreignKeys, $primaryKey);

        //input harus sudah terformat berdasarkan urutan
            foreach ($input as $key => $value) {
                //jika id hashed merupakan array / multi value
                    if(in_array($key, $foreignKeys)){
                        if(is_array($value)){
                            $i = 0;
                            foreach($value as $k => $v){
                                $decode = $this->decode($v);
                                $input_processed[$key][] = $decode;
                                $i++;
                            }
                            
                            $input_processed[$key] = array_values($input_processed[$key]);
                        }
                        else{
                            $decode = $this->decode($value);
                            $input_processed[$key] = $decode;
                        }
                    }
            }
    
        //hapus bidang usaha
            if($this->delClassification($input['id_rekanan'])){
                //insert data bidang usaha baru
                    $i = 0;
                    foreach($input_processed['id_bidang_usaha'] as $bidang_usaha => $value){
                        $data_input = [
                            'id_rekanan' => $input_processed['id_rekanan'],
                            'id_bidang_usaha' => $value,
                            'id_bidang_usaha_detail' => $input_processed['id_bidang_usaha_detail'][$i],
                            'keterangan' => $input['keterangan']
                        ];

                        RekananBidangUsaha::create($data_input);

                    }

                $message = env('RESPONSE_UPDATE_SUCCESS');
                return renderResponse($input, true, $message);
            }

        //if update failed
            $message = env('RESPONSE_UPDATE_FAILED');
            return renderResponse($input, false, $message);  
    } 

    public function updateDocumentVendor(){
        $input = $request->input();
        
        //validation
            $rules = [
                'id_rekanan' => 'required',
                'id_dokumen_perusahaan' => 'required',
                'keterangan' => 'required',
                'tanggal_mulai' => 'required', 
                'tanggal_selesai' => 'required', 
                'file_path' => 'required'
            ];

            $validator = Validator::make($input, $rules);

            if($validator->fails()){
                $error = $validator->messages()->toJson();
                return renderResponse($input, false, $error);
            }

        $model = new RekananDokumen();
        $foreignKeys = $model->getForeignKeys();
        $primaryKey = $model->getKeyName();

        $data = $model->encodeForeignKeys($model, $input);

        $RekananDokumen = RekananDokumen::find($data['unhashed'][$primaryKey]);
        $fillable_keys = is_null($model->getFillable())?[]:$model->getFillable();
        
        if(is_array($fillable_keys)){
            array_push($fillable_keys, $primaryKey);
        }else{
            $fillable_keys = array($fillable_keys, $primaryKey);
        }

        foreach($fillable_keys as $f => $v){
            $RekananDokumen->$v = $data['unhashed'][$v];
        }
        
        $save = $RekananDokumen->save();

        if(!$save){
            $message = env('RESPONSE_UPDATE_FAILED');
            return renderResponse($data['input'], false, $message);
        }

        $message = env('RESPONSE_UPDATE_SUCCESS');
        return renderResponse($data['input'], true, $message);
    }

    public function updateVendorLokal(Request $request){
        $input = $request->input();

        //validation 
            $rules = [
                //rekanan_lokal
                    'rekanan_lokal.id_rekanan' => 'required',
                    'rekanan_lokal.nama_perusahaan' => 'required',
                    'rekanan_lokal.id_tipe_perusahaan' => 'required',
                    'rekanan_lokal.id_jenis_perusahaan' => 'required',
                    'rekanan_lokal.detail_alamat' => 'required',
                    'rekanan_lokal.id_provinsi' => 'required',
                    'rekanan_lokal.id_kota' => 'required',
                    'rekanan_lokal.id_kecamatan' => 'required',
                    'rekanan_lokal.email' => 'required',
                    'rekanan_lokal.telepon' => 'required',
                    'rekanan_lokal.fax' => 'required',
                    'rekanan_lokal.website' => 'required',
                //rekanan_pic
                    'rekanan_pic.nama_depan' => 'required',
                    'rekanan_pic.nama_belakang' => 'required',
                    'rekanan_pic.jabatan' => 'required',
                    'rekanan_pic.email' => 'required',
                    'rekanan_pic.telepon' => 'required',
                    'rekanan_pic.mobile_phone' => 'required',
                    'rekanan_pic.alamat' => 'required'
            ];


        $validator = Validator::make($input, $rules);

            if($validator->fails()){
                $error = $validator->messages()->toJson();
                return renderResponse($input, false, $error);
            }

        $model = new RekananLokal();
        $foreignKeys['rekanan_lokal'] = $model->getForeignKeys();
        $primaryKey = $model->getKeyName();

        $model2 = new RekananPic();
        $primaryKey2 = $model2->getKeyName();
        $foreignKeys2['rekanan_pic'] = $model2->getForeignKeys();

        $FK = array_merge($foreignKeys, $foreignKeys2);

        //cek kalau ada input yang di hash
            $input_processed = $input;
            foreach ($input as $key => $value) {
                //array multi table
                    foreach($value as $v => $k){
                        //jika id hashed merupakan array / multi value
                            if(in_array($v, $FK[$key])){
                                if(is_array($k)){
                                    $i = 0;
                                    foreach($k as $k2 => $v2){
                                        $decode = $this->decode($v2);
                                        $input_processed[$key][$v][$i] = $decode;
                                        $i++;
                                    }
                                    //fix indexing array
                                        $input_processed[$key][$v] = array_values($input_processed[$key][$v]);
                                }
                                else{
                                    $decode = $this->decode($k);
                                    $input_processed[$key][$v] = $decode;
                                }
                            } 
                    }
            }

        //update table rekanan_lokal
            $RekananLokal = RekananLokal::where('id_rekanan', $input_processed['rekanan_lokal']['id_rekanan']);
            $fillable_keys = is_null($model->getFillable())?[]:$model->getFillable();

            $data_input = [];

            foreach($fillable_keys as $f => $v){
                $data_input[$v] = $input_processed['rekanan_lokal'][$v];
            }
            
            $save = $RekananLokal->update($data_input);

            if(!$save){
                $message = env('RESPONSE_UPDATE_FAILED');
                return renderResponse($input, false, $message);
            }

        //delete rekanan_pic
            $this->delPic($input['rekanan_lokal']['id_rekanan']);

        //insert table rekanan_pic
            $i = 0;
            foreach($input['rekanan_pic']['nama_depan'] as $f => $v){
                $data_input = [
                    'id_rekanan' => $input_processed['rekanan_lokal']['id_rekanan'],
                    'nama_depan' => $input_processed['rekanan_pic']['nama_depan'][$i], 
                    'nama_belakang' => $input_processed['rekanan_pic']['nama_belakang'][$i],
                    'jabatan' => $input_processed['rekanan_pic']['jabatan'][$i],
                    'email' => $input_processed['rekanan_pic']['email'][$i],
                    'telepon' => $input_processed['rekanan_pic']['telepon'][$i],
                    'mobile_phone' => $input_processed['rekanan_pic']['mobile_phone'][$i],
                    'alamat' => $input_processed['rekanan_pic']['alamat'][$i],
                ];

                $save = RekananPic::create($data_input);
                unset($data_input);
                $i++;
            }
            
            if(!$save){
                $message = env('RESPONSE_UPDATE_FAILED');
                return renderResponse($input, false, $message);
            }

        $message = env('RESPONSE_UPDATE_SUCCESS');
        return renderResponse($input, true, $message);
    }

    public function delClassification($id_vendor){
        //fungsi untuk hapus klasifikasi vendor (untuk keperluan update)
            $id_rekanan = $this->decode($id_vendor);
            
            if(!isset($id_rekanan)){
                $error = env('RESPONSE_NO_DATA');
                return renderResponse($id_vendor, false, $error);
            }

        //search
            $RekananBidangUsaha = RekananBidangUsaha::where('id_rekanan', $id_rekanan);

        //delete
            if ($RekananBidangUsaha->delete()) {
                $message = env('RESPONSE_DEL_SUCCESS');
                return true;
            }

            return false;
    }

    public function delPic($id_vendor){
        //fungsi untuk hapus klasifikasi vendor (untuk keperluan update)
            $id_rekanan = $this->decode($id_vendor);
            
            if(!isset($id_rekanan)){
                $error = env('RESPONSE_NO_DATA');
                return renderResponse($id_vendor, false, $error);
            }

        //search
            $RekananPic = RekananPic::where('id_rekanan', $id_rekanan);

        //delete
            if ($RekananPic->delete()) {
                $message = env('RESPONSE_DEL_SUCCESS');
                return true;
            }

            return false;
    }

    public function suspendVendorLokal(Request $request){
        $input = $request->input();

        $id_rekanan = $this->decode($input['id_rekanan']);
            
            if(!isset($id_rekanan)){
                $error = env('RESPONSE_NO_DATA');
                return renderResponse($id_vendor, false, $error);
            }
        
        //validation
            $rules = [
                'id_rekanan' => 'required',
                'suspend' => 'required',
                'keterangan_suspend' => 'required'
            ];

            $validator = Validator::make($input, $rules);

            if($validator->fails()){
                $error = $validator->messages()->toJson();
                return renderResponse($input, false, $error);
            }

        $id_rekanan = $this->decode($input['id_rekanan']);
        $RekananLokal = RekananLokal::where('id_rekanan', $id_rekanan);

        $data_input = Arr::only($input, ['suspend','keterangan_suspend']);
        $save = $RekananLokal->update($data_input);

        if(!$save){
            $message = env('RESPONSE_UPDATE_FAILED');
            return renderResponse($input, false, $message);
        }

        $message = env('RESPONSE_UPDATE_SUCCESS');
        return renderResponse($input, true, $message);
    }

}
