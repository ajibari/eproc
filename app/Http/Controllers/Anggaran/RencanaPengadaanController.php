<?php

namespace App\Http\Controllers\Anggaran;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use App\Model\Anggaran\RencanaPengadaan;
use App\Exception\Handler;

class RencanaPengadaanController extends Controller
{
    
    public function get($hashed_id){

        $id = $this->decode($hashed_id);
        $error = env('RESPONSE_NO_DATA');

        if(!isset($id[0])){
            return renderResponse($hashed_id, false, $error);
        }
    
        $result = RencanaPengadaan::with(['organisasi_perusahaan','bidang_usaha','bidang_usaha_detail','satuan','mata_uang'])->find($id[0]);

        if(!$result){
            return renderResponse($hashed_id, false, $error);
        }

        $success = env('RESPONSE_GET_DATA');
        return renderResponse($result, true, $success);
    }

    public function get_all(){

        $model = new RencanaPengadaan();
        $primaryKey = $model->getKeyName();

        $result = RencanaPengadaan::with(['organisasi_perusahaan','bidang_usaha','bidang_usaha_detail','satuan','mata_uang'])->get();
        $message = env('RESPONSE_GET_SUCCESS');

        if(!isset($result[0]->$primaryKey)){
            $message = env('RESPONSE_GET_FAILED');
            $result = null;
            return renderResponse($result, false, $message);
        }

        $message = env('RESPONSE_GET_DATA');
        return renderResponse($result, true, $message);
    }

    public function get_by(Request $request, $param, $value){
        
        $form = $request->input();
        $error = env('RESPONSE_NO_DATA');

        $model = new RencanaPengadaan();
        $foreignKeys = is_null($model->getForeignKeys())?[]:$model->getForeignKeys();
        $primaryKey = $model->getKeyName();

        array_push($foreignKeys, $primaryKey);
        
        if(in_array($param, $foreignKeys)){
            $data_decode = $this->decode($value);

            if(!isset($data_decode[0])){
                return renderResponse($param.'='.$value, false, $error);
            }

            $value_unhashed = $data_decode[0];
        }        

        $result = RencanaPengadaan::where($param, '=', $value_unhashed)->with(['organisasi_perusahaan','bidang_usaha','bidang_usaha_detail','satuan','mata_uang'])->get();

        if(!isset($result[0]->$primaryKey)){
            return renderResponse($param.' = '.$value, false, $error);
        }

        $success = env('RESPONSE_GET_DATA');
        return renderResponse($result, true, $success);
    }

    public function get_limit($start, $limit){

        $model = new RencanaPengadaan();
        $primaryKey = $model->getKeyName();

        $result = RencanaPengadaan::offset($start)->limit($limit)->with(['organisasi_perusahaan','bidang_usaha','bidang_usaha_detail','satuan','mata_uang'])->get();
        $message = env('RESPONSE_GET_SUCCESS');

        if(!isset($result[0]->$primaryKey)){
            $message = env('RESPONSE_GET_FAILED');
            $result = null;
            return renderResponse($result, false, $message);
        }

        $message = env('RESPONSE_GET_DATA');
        return renderResponse($result, true, $message);
    }

    public function create(Request $request){
        $input = $request->input();

        //validation
            $rules = [
                'id_organisasi_perusahaan' => 'required', 
				'kode_rencana_pengadaan' => 'required', 
				'jenis_pengadaan' => 'required', 
				'nama_rencana_pengadaan' => 'required', 
				'lokasi' => 'required', 
				'tanggal_permintaan' => 'required', 
				'tanggal_mulai_kebutuhan' => 'required', 
				'tanggal_selesai_kebutuhan' => 'required', 
				'status' => 'required', 
				'approved_by' => 'required', 
				'tanggal_status' => 'required'
            ];

            $validator = Validator::make($input, $rules);

            if($validator->fails()){
                $error = $validator->messages()->toJson();
                return renderResponse($input, false, $error);
            }
        
        //encode hashing
            $model = new RencanaPengadaan();
            $foreignKeys = is_null($model->getForeignKeys())?[]:$model->getForeignKeys();

            if(!empty($foreignKeys)){
                $method = 'create';
                $data = $model->encodeForeignKeys($model, $input, $method);
                $data_input = $data['unhashed'];
            }else{
                $data_input = $input;
            }

        RencanaPengadaan::create($data_input);
        $message = env('RESPONSE_SAVE_SUCCESS');
        return renderResponse($input, true, $message);
    }

    public function update(Request $request){
        
        $input = $request->input();
        
        //validation
            $rules = [
                'id_pengadaan' => 'required',
                'id_organisasi_perusahaan' => 'required', 
				'kode_rencana_pengadaan' => 'required', 
				'jenis_pengadaan' => 'required', 
				'nama_rencana_pengadaan' => 'required', 
				'lokasi' => 'required', 
				'tanggal_permintaan' => 'required', 
				'tanggal_mulai_kebutuhan' => 'required', 
				'tanggal_selesai_kebutuhan' => 'required', 
				'status' => 'required', 
				'approved_by' => 'required', 
				'tanggal_status' => 'required'
            ];  

            $validator = Validator::make($input, $rules);

            if($validator->fails()){
                $error = $validator->messages()->toJson();
                return renderResponse($input, false, $error);
            }
        
        $model = new RencanaPengadaan();
        $foreignKeys = $model->getForeignKeys();
        $primaryKey = $model->getKeyName();

        $data = $model->encodeForeignKeys($model, $input);

        $RencanaPengadaan = RencanaPengadaan::find($data['unhashed'][$primaryKey]);

        $fillable_keys = is_null($model->getFillable())?[]:$model->getFillable();
        
        if(is_array($fillable_keys)){
            array_push($fillable_keys, $primaryKey);
        }else{
            $fillable_keys = array($fillable_keys, $primaryKey);
        }

        foreach($fillable_keys as $f => $v){
            $RencanaPengadaan->$v = $data['unhashed'][$v];
        }
        
        $save = $RencanaPengadaan->save();

        if(!$save){
            $message = env('RESPONSE_UPDATE_FAILED');
            return renderResponse($data['input'], false, $message);
        }

        $message = env('RESPONSE_UPDATE_SUCCESS');
        return renderResponse($data['input'], true, $message);
    }

    public function del(Request $request){
        
        $model = new RencanaPengadaan();

        $input = $request->input();
        $primaryKey = $model->getKeyName();
        $id = $this->decode($input[$primaryKey]);
        
        //search
            $anggaran = RencanaPengadaan::find($id[0]);

        //delete
            if ($anggaran->delete($id)) {
                $message = env('RESPONSE_DEL_SUCCESS');
                return renderResponse($input, true, $message);
            }

            $message = env('RESPONSE_DEL_FAILED');
            return renderResponse($input, false, $message);
    }
}
