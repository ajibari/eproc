<?php

namespace App\Http\Controllers\Anggaran;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use App\Model\Anggaran\PermintaanPesanan;
use App\Model\Anggaran\PermintaanPesananItem;
use App\Model\Anggaran\RencanaPengadaan;
use App\Model\Anggaran\RencanaPengadaanItem;
use App\Exception\Handler;

class PermintaanPesananController extends Controller
{
    
    public function get($hashed_id){

        $id = $this->decode($hashed_id);
        $error = env('RESPONSE_NO_DATA');

        if(!isset($id[0])){
            return renderResponse($hashed_id, false, $error);
        }
    
        $result = PermintaanPesanan::with(['item','organisasi_perusahaan'])->find($id[0]);

        if(!$result){
            return renderResponse($hashed_id, false, $error);
        }

        $success = env('RESPONSE_GET_DATA');
        return renderResponse($result, true, $success);
    }

    public function get_all(){

        $model = new PermintaanPesanan();
        $primaryKey = $model->getKeyName();

        $result = PermintaanPesanan::with(['item','organisasi_perusahaan'])->get();
        $message = env('RESPONSE_GET_SUCCESS');

        if(!isset($result[0]->$primaryKey)){
            $message = env('RESPONSE_GET_FAILED');
            $result = null;
            return renderResponse($result, false, $message);
        }

        $message = env('RESPONSE_GET_DATA');
        return renderResponse($result, true, $message);
    }

    public function get_by(Request $request, $param, $value){
        
        $form = $request->input();
        $error = env('RESPONSE_NO_DATA');

        $model = new PermintaanPesanan();
        $foreignKeys = is_null($model->getForeignKeys())?[]:$model->getForeignKeys();
        $primaryKey = $model->getKeyName();

        array_push($foreignKeys, $primaryKey);
        
        if(in_array($param, $foreignKeys)){
            $data_decode = $this->decode($value);

            if(!isset($data_decode[0])){
                return renderResponse($param.'='.$value, false, $error);
            }

            $value_unhashed = $data_decode[0];
        }        

        $result = PermintaanPesanan::with(['item','organisasi_perusahaan'])->where($param, '=', $value_unhashed)->get();

        if(!isset($result[0]->$primaryKey)){
            return renderResponse($param.' = '.$value, false, $error);
        }

        $success = env('RESPONSE_GET_DATA');
        return renderResponse($result, true, $success);
    }

    public function get_limit($start, $limit){

        $model = new PermintaanPesanan();
        $primaryKey = $model->getKeyName();

        $result = PermintaanPesanan::with(['item','organisasi_perusahaan'])->offset($start)->limit($limit)->get();
        $message = env('RESPONSE_GET_SUCCESS');

        if(!isset($result[0]->$primaryKey)){
            $message = env('RESPONSE_GET_FAILED');
            $result = null;
            return renderResponse($result, false, $message);
        }

        $message = env('RESPONSE_GET_DATA');
        return renderResponse($result, true, $message);
    }

    public function create(Request $request){
        $input = $request->input();

        //validation
            $rules = [
                'id_organisasi_perusahaan' => 'required', 
                'kode_permintaan_pesanan' => 'required', 
                'nama_permintaan_pesanan' => 'required', 
                'lokasi' => 'required', 
                'tanggal_permintaan' => 'required',
                'tanggal_kebutuhan' => 'required', 
                'status' => 'required', 
                'tanggal_status' => 'required', 
                'requested_by' => 'required', 
                'supervisor' => 'required'
            ];

            $validator = Validator::make($input, $rules);

            if($validator->fails()){
                $error = $validator->messages()->toJson();
                return renderResponse($input, false, $error);
            }
        
        //encode hashing
            $model = new PermintaanPesanan();
            $foreignKeys = is_null($model->getForeignKeys())?[]:$model->getForeignKeys();

            if(!empty($foreignKeys)){
                $method = 'create';
                $data = $model->encodeForeignKeys($model, $input, $method);
                $data_input = $data['unhashed'];
            }else{
                $data_input = $input;
            }

        PermintaanPesanan::create($data_input);
        $message = env('RESPONSE_SAVE_SUCCESS');
        return renderResponse($input, true, $message);
    }

    public function update(Request $request){
        
        $input = $request->input();
        
        //validation
            $rules = [
                'id_permintaan_pesanan' => 'required',
                'id_organisasi_perusahaan' => 'required', 
                'kode_permintaan_pesanan' => 'required', 
                'nama_permintaan_pesanan' => 'required', 
                'lokasi' => 'required', 
                'tanggal_permintaan' => 'required',
                'tanggal_kebutuhan' => 'required', 
                'status' => 'required', 
                'tanggal_status' => 'required', 
                'requested_by' => 'required', 
                'supervisor' => 'required'
            ];  

            $validator = Validator::make($input, $rules);

            if($validator->fails()){
                $error = $validator->messages()->toJson();
                return renderResponse($input, false, $error);
            }
        
        $model = new PermintaanPesanan();
        $foreignKeys = $model->getForeignKeys();
        $primaryKey = $model->getKeyName();

        $data = $model->encodeForeignKeys($model, $input);

        $permintaanPesanan = PermintaanPesanan::find($data['unhashed'][$primaryKey]);

        $fillable_keys = is_null($model->getFillable())?[]:$model->getFillable();
        
        if(is_array($fillable_keys)){
            array_push($fillable_keys, $primaryKey);
        }else{
            $fillable_keys = array($fillable_keys, $primaryKey);
        }

        foreach($fillable_keys as $f => $v){
            $permintaanPesanan->$v = $data['unhashed'][$v];
        }
        
        $save = $PermintaanPesanan->save();

        if(!$save){
            $message = env('RESPONSE_UPDATE_FAILED');
            return renderResponse($data['input'], false, $message);
        }

        $message = env('RESPONSE_UPDATE_SUCCESS');
        return renderResponse($data['input'], true, $message);
    }

    public function del(Request $request){
        
        $model = new PermintaanPesanan();

        $input = $request->input();
        $primaryKey = $model->getKeyName();
        $id = $this->decode($input[$primaryKey]);
        
        //search
            $permintaanPesanan = PermintaanPesanan::find($id[0]);

        //delete
            if ($permintaanPesanan->delete($id)) {
                $message = env('RESPONSE_DEL_SUCCESS');
                return renderResponse($input, true, $message);
            }

            $message = env('RESPONSE_DEL_FAILED');
            return renderResponse($input, false, $message);
    }

    public function moveToPengadaan(Request $request){
        $input = $request->input();

        $model = new PermintaanPesanan();
        $model2 = new PermintaanPesananItem();

        $model3 = new RencanaPengadaan();
        $model4 = new RencanaPengadaanItem();

        $primaryKey = $model->getKeyName();

        $id = $this->decode($input[$primaryKey]);

        $data = $model->with(['item','organisasi_perusahaan'])->where($primaryKey, $id[0])->get()->makeVisible($model->getKeyName())->makeVisible($model->getForeignKeys())->makeVisible($model2->getKeyName())->makeVisible($model2->getForeignKeys())->toArray();

        if(isset($data[0]['id'][$primaryKey])){

            if(!$data[0]['status']){
                $message = env('RESPONSE_ADD_FAILED');
                return renderResponse($input, true, $message);
            }

            $input_data = [
                'id_organisasi_perusahaan' => $data[0]['id_organisasi_perusahaan'],
                'kode_rencana_pengadaan' => $data[0]['kode_permintaan_pesanan'],
                'jenis_pengadaan' => null,
                'nama_rencana_pengadaan' => $data[0]['nama_permintaan_pesanan'],
                'lokasi' => $data[0]['lokasi'],
                'tanggal_permintaan' => $data[0]['tanggal_permintaan'],
                'tanggal_mulai_kebutuhan' => $data[0]['tanggal_kebutuhan'],
                'status' => $data[0]['status'],
                'approved_by' => 1,
                'tanggal_status' => $data[0]['tanggal_status']
            ];

            $RencanaPengadaan = $model3::create($input_data);

            foreach($data[0]['item'] as $key){

                $primaryKey1 = $model3->getKeyName();
                $id_bidang_usaha = $this->decode($key['id']['id_bidang_usaha']);
                $id_bidang_usaha_detail = $this->decode($key['id']['id_bidang_usaha_detail']);
                $id_satuan = $this->decode($key['id']['id_satuan']);
                $id_mata_uang = $this->decode($key['id']['id_mata_uang']);

                $input_data2 = [
                    'id_pengadaan' => $RencanaPengadaan->$primaryKey1,
                    'kode_sumberdaya' => null,
                    'id_bidang_usaha' => $id_bidang_usaha[0],
                    'id_bidang_usaha_detail' => $id_bidang_usaha_detail[0],
                    'nama_item' => $key['nama_item'],
                    'spesifikasi_item' => $key['spesifikasi_item'],
                    'volume' => $key['volume'],
                    'harga_satuan' => $key['harga_satuan'],
                    'id_satuan' => $id_satuan[0],
                    'id_mata_uang' => $id_mata_uang[0],
                ];

                //kalau pake $model4::create produce double query execution, buggy
                $model4->insert($input_data2);
                unset($input_data2);
            }
        
        }        

        $message = env('RESPONSE_ADD_SUCCESS');
        return renderResponse($input, true, $message);
    }
}
