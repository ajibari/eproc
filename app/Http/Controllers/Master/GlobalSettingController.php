<?php

namespace App\Http\Controllers\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use App\Model\Master\GlobalSetting;
use App\Exception\Handler;

class GlobalSettingController extends Controller
{

    public function get(){
        $global_setting = new GlobalSetting;
        $rs = $global_setting->get();
        $data = array();
        $no = 0;
        foreach ($rs as $row) {
            $data[$no]['id_global_setting'] = $row->id_global_setting;
            $data[$no]['nama_parameter'] = $row->nama_parameter;
            $data[$no]['nilai_parameter'] = $row->nilai_parameter;
            $data[$no]['keterangan'] = $row->keterangan;
            $no++;
        }
        return renderResponse($data, true, "");
    }

    public function create(Request $request){
    	$input = $request->input();

        //validation
        $rules = [
            'nama_parameter' => 'required|max:250',
            'nilai_parameter' => 'required|max:250',
        ];
        $validator = Validator::make($input, $rules);

        if($validator->fails()){
            $error = $validator->messages()->toJson();
            return renderResponse($input, false, $error);
        }
        
        GlobalSetting::create($input);
           
        return renderResponse($input, true, "");
    }

    public function update(Request $request){
        $input = $request->input();
        //validation
        $rules = [
            'id_global_setting' => 'required',
            'nama_parameter' => 'required|max:250',
            'nilai_parameter' => 'required|max:250'
        ];
        $validator = Validator::make($input, $rules);

        if($validator->fails()){
            $error = $validator->messages()->toJson();
            return renderResponse($input, false, $error);
        }
        
        $global_setting = GlobalSetting::find($input['id_global_setting']);
        if($global_setting->update($input)){
            return renderResponse($input, true, 'Success update data!');
        }
    }

    public function del(Request $request){
        $input = $request->input();
        $id = $input['id_global_setting'];
        $global_setting = GlobalSetting::find($id);
        if ($global_setting->delete($id)) {
            return renderResponse($input, true, "");
        }
    }

    public function get_by(Request $request){
        $input = $request->input();

        //validation
        $rules = [
            'id_global_setting' => 'required'
        ];
        $validator = Validator::make($input, $rules);

        if($validator->fails()){
            $error = $validator->messages()->toJson();
            return renderResponse($input, false, $error);
        }
        
        $global_setting = GlobalSetting::find($input['id_global_setting']);
        $data = array();
        if (count($global_setting)>0){
            $data['id_global_setting'] = $global_setting->id_global_setting;
            $data['nama_parameter'] = $global_setting->nama_parameter;
            $data['nilai_parameter'] = $global_setting->nilai_parameter;
            $data['keterangan'] = $global_setting->keterangan;
        }
        return renderResponse($data, true, "");
    }

    public function del_bulk(){

    }
}
