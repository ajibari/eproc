<?php

namespace App\Http\Controllers\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use App\Model\Master\Province;
use App\Exception\Handler;
use App\Traits\Trackable;

class ProvinceController extends Controller
{
    use Trackable;

    public function get($hashed_id){

        $id = $this->decode($hashed_id);

        $error = env('RESPONSE_NO_DATA');

        if(!isset($id)){
            return renderResponse($hashed_id, false, $error);
        }
    
        $result = Province::find($id);

        if(!$result){
            return renderResponse($hashed_id, false, $error);
        }

        $success = env('RESPONSE_GET_DATA');
        return renderResponse($result, true, $success);
    }

    public function get_by(Request $request, $param, $value){

        $form = $request->input();
        $error = env('RESPONSE_NO_DATA');

        $provinsi = new Province();
        $foreignKeys = is_null($provinsi->getForeignKeys())?[]:$provinsi->getForeignKeys();
        $primaryKey = $provinsi->getKeyName();

        array_push($foreignKeys, $primaryKey);
        
        if(in_array($param, $foreignKeys)){
            $data_decode = $this->decode($value);

            if(!isset($data_decode)){
                return renderResponse($param.'='.$value, false, $error);
            }

            $value_unhashed = $data_decode;
        }        

        $result = Province::where($param, '=', $value_unhashed)->get();
        
        if(!isset($result[0]->$primaryKey)){
            return renderResponse($param.' = '.$value, false, $error);
        }

        $success = env('RESPONSE_GET_DATA');
        return renderResponse($result, true, $success);
    }

    public function get_all(){

        $model = new Province();
        $primaryKey = $model->getKeyName();

        $result = Province::get();
        $message = env('RESPONSE_GET_SUCCESS');

        if(!isset($result[0]->$primaryKey)){
            $message = env('RESPONSE_GET_FAILED');
            $result = null;
            return renderResponse($result, false, $message);
        }

        $message = env('RESPONSE_GET_DATA');
        return renderResponse($result, true, $message);
    }

    public function get_limit($start, $limit){

        $model = new Province();
        $primaryKey = $model->getKeyName();

        $result = Province::offset($start)->limit($limit)->get();
        $message = env('RESPONSE_GET_SUCCESS');

        if(!isset($result[0]->$primaryKey)){
            $message = env('RESPONSE_GET_FAILED');
            $result = null;
            return renderResponse($result, false, $message);
        }

        $message = env('RESPONSE_GET_DATA');
        return renderResponse($result, true, $message);
    }

    public function create(Request $request){
        $input = $request->input();

        //validation
        $rules = [
            'nama_provinsi' => 'required|max:200',
        ];
        $validator = Validator::make($input, $rules);

        if($validator->fails()){
            $error = $validator->messages()->toJson();
            return renderResponse($input, false, $error);
        }
        
        //encode hashing
            $model = new Province();
            $foreignKeys = is_null($model->getForeignKeys())?[]:$model->getForeignKeys();
            
            if(!empty($foreignKeys)){
                $method = 'create';
                $data = $model->encodeForeignKeys($model, $input, $method);
                $data_input = $data['unhashed'];
            }else{
                $data_input = $input;
            }
            
        Province::create($data_input);
        $message = env('RESPONSE_SAVE_SUCCESS');
        return renderResponse($input, true, $message);
    }

    public function update(Request $request){
       
        $input = $request->input();

        //validation
            $rules = [
                'id_provinsi' => 'required',
                'nama_provinsi' => 'required|max:200'
            ];

            $validator = Validator::make($input, $rules);

            if($validator->fails()){
                $error = $validator->messages()->toJson();
                return renderResponse($input, false, $error);
            }
        
        $model = new Province();
        $foreignKeys = $model->getForeignKeys();
        $primaryKey = $model->getKeyName();

        $data = $model->encodeForeignKeys($model, $input);
        $Province = Province::find($data['unhashed'][$primaryKey]);
        $fillable_keys = is_null($model->getFillable())?[]:$model->getFillable();
        
        if(is_array($fillable_keys)){
            array_push($fillable_keys, $primaryKey);
        }else{
            $fillable_keys = array($fillable_keys, $primaryKey);
        }

        foreach($fillable_keys as $f => $v){
            $Province->$v = $data['unhashed'][$v];
        }
        
        $save = $Province->save();

        if(!$save){
            $message = env('RESPONSE_UPDATE_FAILED');
            return renderResponse($data['input'], false, $message);
        }

        $message = env('RESPONSE_UPDATE_SUCCESS');
        return renderResponse($data['input'], true, $message);
    }

    public function del(Request $request){
        
        $model = new Province();
        
        $primaryKey = $model->getKeyName();
        $input = $request->input();
        $id = $this->decode($input[$primaryKey]);

        //search
            $province = Province::find($id);

        //delete
            $del = $province->delCheckRelationship($province);

            if($del !== true){
                return renderResponse($input, false, $del);
            }

            $message = env('RESPONSE_DEL_SUCCESS');
            return renderResponse($input, true, $message);
    }

    public function del_bulk(){

    }
}
