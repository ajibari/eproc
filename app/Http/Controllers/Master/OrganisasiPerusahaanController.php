<?php

namespace App\Http\Controllers\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Master\OrganisasiPerusahaan;
use Validator;

class OrganisasiPerusahaanController extends Controller
{
    public function get($hashed_id){
        
        $id = $this->decode($hashed_id);

        $error = env('RESPONSE_NO_DATA');

        if(!isset($id)){
            return renderResponse($hashed_id, false, $error);
        }
    
        $result = OrganisasiPerusahaan::find($id);

        if(!$result){
            return renderResponse($hashed_id, false, $error);
        }

        $success = env('RESPONSE_GET_DATA');
        return renderResponse($result, true, $success);
    }

    public function get_all(){

    	$model = new OrganisasiPerusahaan();
        $primaryKey = $model->getKeyName();

        $result = OrganisasiPerusahaan::orderBy('kode_organisasi_perusahaan', 'asc')->get();
        $message = env('RESPONSE_GET_SUCCESS');

        if(!isset($result[0]->$primaryKey)){
            $message = env('RESPONSE_GET_FAILED');
            $result = null;
            return renderResponse($result, false, $message);
        }

        $message = env('RESPONSE_GET_DATA');
        return renderResponse($result, true, $message);
    }

    public function get_limit($start, $limit){

    	$model = new OrganisasiPerusahaan();
        $primaryKey = $model->getKeyName();

        $result = OrganisasiPerusahaan::offset($start)->limit($limit)->get();
        $message = env('RESPONSE_GET_SUCCESS');

        if(!isset($result[0]->$primaryKey)){
            $message = env('RESPONSE_GET_FAILED');
            $result = null;
            return renderResponse($result, false, $message);
        }

        $message = env('RESPONSE_GET_DATA');
        return renderResponse($result, true, $message);
    }

    public function get_by(Request $request, $param, $value){

        $form = $request->input();
        $error = env('RESPONSE_NO_DATA');

        $model = new OrganisasiPerusahaan();
        $foreignKeys = is_null($model->getForeignKeys())?[]:$model->getForeignKeys();
        $primaryKey = $model->getKeyName();

        array_push($foreignKeys, $primaryKey);
        
        if(in_array($param, $foreignKeys)){
            $data_decode = $this->decode($value);

            if(!isset($data_decode)){
                return renderResponse($param.'='.$value, false, $error);
            }

            $value_unhashed = $data_decode;
        }        

        $result = OrganisasiPerusahaan::where($param, '=', $value_unhashed)->with('provinsi')->get();
        
        if(!isset($result[0]->$primaryKey)){
            return renderResponse($param.' = '.$value, false, $error);
        }

        $success = env('RESPONSE_GET_DATA');
        return renderResponse($result, true, $success);
    }

    public function create(Request $request){
    	$input = $request->input();

        //validation
        $rules = [
            'kode' => 'required',
            'kode_induk' => 'required',
            'kode_organisasi_perusahaan' => 'required',
            'nama_organisasi_perusahaan' => 'required',
        ];

        $validator = Validator::make($input, $rules);

        if($validator->fails()){
            $error = $validator->messages()->toJson();
            return renderResponse($input, false, $error);
        }
        
        //encode hashing
            $model = new OrganisasiPerusahaan();
        	$foreignKeys = is_null($model->getForeignKeys())?[]:$model->getForeignKeys();
        	
        	if(!empty($foreignKeys)){
        		$method = 'create';
            	$data = $model->encodeForeignKeys($model, $input, $method);
            	$data_input = $data['unhashed'];
        	}else{
        		$data_input = $input;
        	}
        
        OrganisasiPerusahaan::create($data_input);
        $message = env('RESPONSE_SAVE_SUCCESS');
        return renderResponse($input, true, $message);
    }

    public function update(Request $request){
        
        $input = $request->input();
        
        //validation
            $rules = [
                'kode' => 'required',
	            'kode_induk' => 'required',
	            'kode_organisasi_perusahaan' => 'required',
	            'nama_organisasi_perusahaan' => 'required',
            ];

            $validator = Validator::make($input, $rules);

            if($validator->fails()){
                $error = $validator->messages()->toJson();
                return renderResponse($input, false, $error);
            }

        $model = new OrganisasiPerusahaan();
        $foreignKeys = $model->getForeignKeys();
        $primaryKey = $model->getKeyName();

        $data = $model->encodeForeignKeys($model, $input);
        
        $OrganisasiPerusahaan = OrganisasiPerusahaan::find($data['unhashed'][$primaryKey]);
        $fillable_keys = is_null($model->getFillable())?[]:$model->getFillable();
        
        if(is_array($fillable_keys)){
            array_push($fillable_keys, $primaryKey);
        }else{
            $fillable_keys = array($fillable_keys, $primaryKey);
        }

        foreach($fillable_keys as $f => $v){
            $OrganisasiPerusahaan->$v = $data['unhashed'][$v];
        }
        
        $save = $OrganisasiPerusahaan->save();

        if(!$save){
            $message = env('RESPONSE_UPDATE_FAILED');
            return renderResponse($data['input'], false, $message);
        }

        $message = env('RESPONSE_UPDATE_SUCCESS');
        return renderResponse($data['input'], true, $message);
    }

    public function del(Request $request){

        $model = new OrganisasiPerusahaan();

        $input = $request->input();
        $primaryKey = $model->getKeyName();
        $id = $this->decode($input[$primaryKey]);
        
        //search
            $model = OrganisasiPerusahaan::find($id);

        //delete
            $del = $model->delCheckRelationship($model);

            if($del !== true){
                return renderResponse($input, false, $del);
            }

            $message = env('RESPONSE_DEL_SUCCESS');
            return renderResponse($input, true, $message);
    }
}
