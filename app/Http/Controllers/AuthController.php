<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Model\Master\User;
use App\Model\UserVerification;
use Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Password;
use Illuminate\Mail\Message;
use Illuminate\Support\Facades\DB;
use MongoDB\Client as Mongo;

class AuthController extends Controller
{
    //
    public function register(Request $request){

        $form = $request->input();

    	$rules = [
    		'name' => 'required|max:255',
    		'email' => 'required|email|max:255|unique:users',
    		'password' => 'required|confirmed|min:5'
    	];

    	$input = $request->only(
    		'name',
    		'email',
    		'password',
    		'password_confirmation'
    	);

    	$validator = Validator::make($input, $rules);

    	if($validator->fails()){
    		$error = $validator->messages()->toJson();
    		return response()->json(['success' => false, 'error' => $error]);
    	}

    	$user = User::create(['name' => $form['name'], 'email' => $form['email'], 'password' => Hash::make($form['password']), 'is_verified' => 0]);

    	$verification_code = str_random(30);
        UserVerification::create(['user_id'=>$user->id,'token'=>$verification_code]);
        
        $message = env('RESPONSE_USER_ADD_SUCCESS');
        return renderResponse($input, true, $message);

    }

    public function verifyUser($verification_code){

        $check = DB::table('user_verifications')->where('token', $verification_code)->first();

        if(!is_null($check)){
            $user = User::find($check->user_id);

            if($user->is_verified == 1){
                return response()->json([
                    'success' => true,
                    'message' => 'Account already verified'
                ]);
            }

            $user->update(['is_verified' => 1]);
            DB::table('user_verifications')->where('token',$verification_code)->delete();

            return response()->json([
                'success'=> true,
                'message'=> 'You have successfully verified your email address.'
            ]);
        }

         return response()->json(['success'=> false, 'error'=> "Verification code is invalid."]);

    }

    public function login(Request $request)
    {
        
        $rules = [
            'email' => 'required|email',
            'password' => 'required',
        ];
        

        $input = $request->only('email', 'password');
        $validator = Validator::make($input, $rules);

        if($validator->fails()) {
            $error = $validator->messages()->toJson();
            return response()->json(['success'=> false, 'error'=> $error]);
        }

        $login = User::where('email', $request->input('email'))->first();

        if(!empty($login)){
            if (Hash::check($request->input('password'), $login->password)) {
            
                $api_token = sha1(time());

                $create_token = User::where('id', $login->id)->update(['remember_token' => $api_token]);    
                
                if (!$create_token) {
                    $message = env('RESPONSE_LOGIN_FAILED');
                    return renderResponse($input, false, $message);
                }

                $data = ['user_id' => $login->id, 'api_token' => $api_token];

                setOnline($api_token, $login->id);  //set online in mongo
                counterUser($login->id); //untuk count visitor

                $result = User::where('id', $login->id)->first();

                $message = env('RESPONSE_LOGIN_SUCCESS');
                return renderResponse($result, true, $message);

            }

            //false authentication
            $message = env('RESPONSE_LOGIN_FAILED');
            return renderResponse($input, false, $message);
        }
        

        //false authentication
            $message = env('RESPONSE_LOGIN_FAILED');
            return renderResponse($input, false, $message);
    }
    /**
     * Log out
     * Invalidate the token, so user cannot use it anymore
     * They have to relogin to get a new token
     *
     * @param Request $request
     */
    public function logout(Request $request) {

        $input = $request->header();

        $user = User::where('remember_token', $input['token'])->first();

        if(!$user){
            $message = env('RESPONSE_LOGIN_FAILED');
            return renderResponse('', false, $message);
        }

        $update = $user->update(['remember_token' => null]);

        setOffline($user->id);
        
        $message = env('RESPONSE_LOGOUT_SUCCESS');
        return renderResponse('', true, $message);
    }

}
