<?php

namespace App\Http\Middleware;

use Closure;
use App\Model\Master\User;
use App\Model\Master\RoleUser;

class PermissionMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $permissions, $options = '')
    {
        /**
         * @param token string
         */
        
        $permission = trim($permissions);
        $permissions = explode('|',$permission);
        $token = $request->header('token');

        if (empty($token)){
            echo json_encode(array('message' => 'you don\'t have access'));
            exit;
        }

        $user = User::where('remember_token', '=', $token)->first();
        if(!empty($user->id)){
            $result = RoleUser::find($user->id)->permission()->whereIn('permissions.name',$permissions)->first();

            if(!empty($result->id)){
                return $next($request);
            }
                    
        }

        echo json_encode(array('message' => 'you don\'t have access'));
        exit;
    }
}
