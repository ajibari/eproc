<?php

namespace App\Http\Middleware;

use Closure;
use App\Model\Master\User;

class RoleMiddleware
{
    /**
     * Log a user to the app
     *
     * @param Request $request
     *
     * @ApiDescription(section="App", description="Log the user to the app using Facebook token")
     * @ApiMethod(type="post")
     * @ApiRoute(name="localhost:8000/api/login")
     * @ApiParams(name="email", type="string", description="Registered email in system", sample="{
     *     'email': 'string',
     * }")
     * @ApiParams(name="password", type="string", description="Registered password", sample="{'password': 'password123'}")
     * @ApiReturnHeaders(sample="HTTP 200 OK")
     * @ApiReturn(type="object", sample="{
     *     'success': true,
     *       'remember_token': 'aa63bfa834faa157c5e1e3e5dc21774dc2860c8d',
     *       'message': {
     *           'id': 3,
     *           'name': 'User',
     *           'email': 'user@app.com',
     *           'is_verified': '0',
     *           'created_at': '2017-11-13 10:14:47',
     *           'updated_at': '2017-11-13 10:14:47'
     *       }
     * }")
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function handle($request, Closure $next, $roles, $team = null, $options = '')
    {
        $role = trim($roles);
        $roles = explode('|',$role);
        $token = $request->header('token');

        if (empty($token)){
            echo json_encode(array('message' => 'you don\'t have access'));
            exit;
        }

        $user = User::where('remember_token', '=', $token)->first();

        $result = User::find($user->id)->role()->whereIn('name', $roles)->first();

        if(!empty($result->id)){
            return $next($request);
        }
            
        echo json_encode(array('message' => 'you don\'t have access'));
        exit;
    }
}
