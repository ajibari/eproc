<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
// use App\Model\Master\User;
// use App\Model\Master\Kota;
// use App\Model\Master\Kecamatan;
// use App\Model\Master\Province;
// use App\Services\Observers\UserObserver;
// use App\Services\Observers\KotaObserver;
// use App\Services\Observers\KecamatanObserver;
// use App\Services\Observers\ProvinceObserver;

class ObserverServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        // dd(Auth::id());
        // User::observe(UserObserver::class);
        // Kota::observe(KotaObserver::class);
        // Kecamatan::observe(KecamatanObserver::class);
        // Province::observe(ProvinceObserver::class);
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
