<?php

use Illuminate\Http\Request;
use App\Model\Master\User;
use App\Model\Log;
use MongoDB\Client as Mongo; 

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
| Route::get($uri, $callback);
| Route::post($uri, $callback);
| Route::put($uri, $callback);
| Route::patch($uri, $callback);
| Route::delete($uri, $callback);
*/

/*USER*/
Route::get('master/pengguna',['middleware' => ['permissions:read-users'], 'uses' => 'UserController@get']);
Route::post('master/pengguna/{param}/{value}',['middleware' => ['permissions:read-users'], 'uses' => 'UserController@get_by']);
Route::get('master/pengguna/all',['middleware' => ['permissions:read-users'], 'uses' => 'UserController@get_all']);
Route::put('master/pengguna',['middleware' => ['permissions:update-users'], 'uses' => 'UserController@update']);
Route::delete('master/pengguna',['middleware' => ['permissions:delete-users'], 'uses' => 'UserController@del']);

    
/*SATUAN*/
Route::get('master/satuan',['uses' => 'Master\SatuanController@get_all']);
Route::get('master/satuan/{id}',['uses' => 'Master\SatuanController@get']);
Route::get('master/satuan/get_by/{param}/{value}',['uses' => 'Master\SatuanController@get_by']);
Route::get('master/satuan/{start}/{limit}',['uses' => 'Master\SatuanController@get_limit']);
Route::post('master/satuan',['uses' => 'Master\SatuanController@create']);
Route::put('master/satuan',['uses' => 'Master\SatuanController@update']);
Route::delete('master/satuan',['uses' => 'Master\SatuanController@del']);

/*KOTA*/
Route::get('master/kota',['uses' => 'Master\KotaController@get_all']);
Route::get('master/kota/{start}/{limit}',['uses' => 'Master\KotaController@get_limit']);
Route::get('master/kota/{id}',['uses' => 'Master\KotaController@get']);
Route::get('master/kota/{start}/{limit}',['uses' => 'Master\KotaController@get_limit']);
Route::get('master/kota/get_by/{param}/{id}',['uses' => 'Master\KotaController@get_by']);
Route::post('master/kota',['uses' => 'Master\KotaController@create']);
Route::put('master/kota',['uses' => 'Master\KotaController@update']);
Route::delete('master/kota',['uses' => 'Master\KotaController@del']);
Route::post('master/kota/del_bulk_by_id',['uses' => 'Master\KotaController@del_bulk_by_id']);

/*KECAMATAN*/
Route::get('master/kecamatan',['middleware' => ['permissions:read-sub-district'], 'uses' => 'Master\KecamatanController@get_all']);
Route::get('master/kecamatan/{id}',['uses' => 'Master\KecamatanController@get']);
Route::get('master/kecamatan/get_by/{param}/{value}',['uses' => 'Master\KecamatanController@get_by']);
Route::get('master/kecamatan/{start}/{limit}',['uses' => 'Master\KecamatanController@get_limit']);
Route::post('master/kecamatan',['uses' => 'Master\KecamatanController@create']);
Route::put('master/kecamatan',['uses' => 'Master\KecamatanController@update']);
Route::delete('master/kecamatan',['uses' => 'Master\KecamatanController@del']);

/*PROVINCE*/
Route::get('master/provinsi',['uses' => 'Master\ProvinceController@get_all']);
Route::get('master/provinsi/{id}',['uses' => 'Master\ProvinceController@get']);
Route::get('master/provinsi/get_by/{param}/{value}',['uses' => 'Master\ProvinceController@get_by']);
Route::get('master/provinsi/{start}/{limit}',['uses' => 'Master\ProvinceController@get_limit']);
Route::post('master/provinsi',['uses' => 'Master\ProvinceController@create']);
Route::put('master/provinsi',['uses' => 'Master\ProvinceController@update']);
Route::delete('master/provinsi',['uses' => 'Master\ProvinceController@del']);

/*ORGANISASI PERUSAHAAN */
Route::get('master/organisasi_perusahaan',['uses' => 'Master\OrganisasiPerusahaanController@get_all']);
Route::get('master/organisasi_perusahaan/{id}',['uses' => 'Master\OrganisasiPerusahaanController@get']);
Route::get('master/organisasi_perusahaan/get_by/{param}/{value}',['uses' => 'Master\OrganisasiPerusahaanController@get_by']);
Route::get('master/organisasi_perusahaan/{start}/{limit}',['uses' => 'Master\OrganisasiPerusahaanController@get_limit']);
Route::post('master/organisasi_perusahaan',['uses' => 'Master\OrganisasiPerusahaanController@create']);
Route::put('master/organisasi_perusahaan',['uses' => 'Master\OrganisasiPerusahaanController@update']);
Route::delete('master/organisasi_perusahaan',['uses' => 'Master\OrganisasiPerusahaanController@del']);

/*JABATAN*/
Route::get('master/jabatan',['uses' => 'Master\JabatanController@get_all']);
Route::get('master/jabatan/{id}',['uses' => 'Master\JabatanController@get']);
Route::get('master/jabatan/get_by/{param}/{value}',['uses' => 'Master\JabatanController@get_by']);
Route::get('master/jabatan/{start}/{limit}',['uses' => 'Master\JabatanController@get_limit']);
Route::post('master/jabatan',['uses' => 'Master\JabatanController@create']);
Route::put('master/jabatan',['uses' => 'Master\JabatanController@update']);
Route::delete('master/jabatan',['uses' => 'Master\JabatanController@del']);

/*BIDANG USAHA*/
Route::get('master/bidang_usaha',['uses' => 'Master\BidangUsahaController@get_all']);
Route::get('master/bidang_usaha/{id}',['uses' => 'Master\BidangUsahaController@get']);
Route::get('master/bidang_usaha/get_by/{param}/{value}',['uses' => 'Master\BidangUsahaController@get_by']);
Route::get('master/bidang_usaha/{start}/{limit}',['uses' => 'Master\BidangUsahaController@get_limit']);
Route::post('master/bidang_usaha',['uses' => 'Master\BidangUsahaController@create']);
Route::put('master/bidang_usaha',['uses' => 'Master\BidangUsahaController@update']);
Route::delete('master/bidang_usaha',['uses' => 'Master\BidangUsahaController@del']);

/*BIDANG USAHA DETAIL*/
Route::get('master/bidang_usaha_detail',['uses' => 'Master\BidangUsahaDetailController@get_all']);
Route::get('master/bidang_usaha_detail/{id}',['uses' => 'Master\BidangUsahaDetailController@get']);
Route::get('master/bidang_usaha_detail/get_by/{param}/{value}',['uses' => 'Master\BidangUsahaDetailController@get_by']);
Route::get('master/bidang_usaha_detail/{start}/{limit}',['uses' => 'Master\BidangUsahaDetailController@get_limit']);
Route::post('master/bidang_usaha_detail',['uses' => 'Master\BidangUsahaDetailController@create']);
Route::put('master/bidang_usaha_detail',['uses' => 'Master\BidangUsahaDetailController@update']);
Route::delete('master/bidang_usaha_detail',['uses' => 'Master\BidangUsahaDetailController@del']);

/*CAKUPAN WILAYAH*/
Route::get('master/cakupan_wilayah',['uses' => 'Master\CakupanWilayahController@get_all']);
Route::get('master/cakupan_wilayah/{id}',['uses' => 'Master\CakupanWilayahController@get']);
Route::get('master/cakupan_wilayah/get_by/{param}/{value}',['uses' => 'Master\CakupanWilayahController@get_by']);
Route::get('master/cakupan_wilayah/{start}/{limit}',['uses' => 'Master\CakupanWilayahController@get_limit']);
Route::post('master/cakupan_wilayah',['uses' => 'Master\CakupanWilayahController@create']);
Route::put('master/cakupan_wilayah',['uses' => 'Master\CakupanWilayahController@update']);
Route::delete('master/cakupan_wilayah',['uses' => 'Master\CakupanWilayahController@del']);

/*DOKUMEN PERUSAHAAN*/
Route::get('master/dokumen_perusahaan',['uses' => 'Master\DokumenPerusahaanController@get_all']);
Route::get('master/dokumen_perusahaan/{id}',['uses' => 'Master\DokumenPerusahaanController@get']);
Route::get('master/dokumen_perusahaan/get_by/{param}/{value}',['uses' => 'Master\DokumenPerusahaanController@get_by']);
Route::get('master/dokumen_perusahaan/{start}/{limit}',['uses' => 'Master\DokumenPerusahaanController@get_limit']);
Route::post('master/dokumen_perusahaan',['uses' => 'Master\DokumenPerusahaanController@create']);
Route::put('master/dokumen_perusahaan',['uses' => 'Master\DokumenPerusahaanController@update']);
Route::delete('master/dokumen_perusahaan',['uses' => 'Master\DokumenPerusahaanController@del']);

/*GLOBAL SETTING*/
Route::get('master/global_setting/all',['uses' => 'Master\GlobalSettingController@get']);
Route::post('master/global_setting',['uses' => 'Master\GlobalSettingController@create']);
Route::put('master/global_setting',['uses' => 'Master\GlobalSettingController@update']);
Route::delete('master/global_setting',['uses' => 'Master\GlobalSettingController@del']);
Route::get('master/global_setting',['uses' => 'Master\GlobalSettingController@get_by']);

/*GLOBAL SETTING*/
Route::get('master/jenis_perusahaan',['uses' => 'Master\JenisPerusahaanController@get_all']);
Route::get('master/jenis_perusahaan/{id}',['uses' => 'Master\JenisPerusahaanController@get']);
Route::get('master/jenis_perusahaan/get_by/{param}/{value}',['uses' => 'Master\JenisPerusahaanController@get_by']);
Route::get('master/jenis_perusahaan/{start}/{limit}',['uses' => 'Master\JenisPerusahaanController@get_limit']);
Route::post('master/jenis_perusahaan',['uses' => 'Master\JenisPerusahaanController@create']);
Route::put('master/jenis_perusahaan',['uses' => 'Master\JenisPerusahaanController@update']);
Route::delete('master/jenis_perusahaan',['uses' => 'Master\JenisPerusahaanController@del']);

/*KRITERIA PENILAIAN*/
Route::get('master/kriteria_penilaian',['uses' => 'Master\KriteriaPenilaianController@get_all']);
Route::get('master/kriteria_penilaian/{id}',['uses' => 'Master\KriteriaPenilaianController@get']);
Route::get('master/kriteria_penilaian/get_by/{param}/{value}',['uses' => 'Master\KriteriaPenilaianController@get_by']);
Route::get('master/kriteria_penilaian/{start}/{limit}',['uses' => 'Master\KriteriaPenilaianController@get_limit']);
Route::post('master/kriteria_penilaian',['uses' => 'Master\KriteriaPenilaianController@create']);
Route::put('master/kriteria_penilaian',['uses' => 'Master\KriteriaPenilaianController@update']);
Route::delete('master/kriteria_penilaian',['uses' => 'Master\KriteriaPenilaianController@del']);

/*KUALIFIKASI PERUSAHAAN*/
Route::get('master/kualifikasi_perusahaan',['uses' => 'Master\KualifikasiPerusahaanController@get_all']);
Route::get('master/kualifikasi_perusahaan/{id}',['uses' => 'Master\KualifikasiPerusahaanController@get']);
Route::get('master/kualifikasi_perusahaan/get_by/{param}/{value}',['uses' => 'Master\KualifikasiPerusahaanController@get_by']);
Route::get('master/kualifikasi_perusahaan/{start}/{limit}',['uses' => 'Master\KualifikasiPerusahaanController@get_limit']);
Route::post('master/kualifikasi_perusahaan',['uses' => 'Master\KualifikasiPerusahaanController@create']);
Route::put('master/kualifikasi_perusahaan',['uses' => 'Master\KualifikasiPerusahaanController@update']);
Route::delete('master/kualifikasi_perusahaan',['uses' => 'Master\KualifikasiPerusahaanController@del']);

/*MATA UANG*/
Route::get('master/mata_uang',['uses' => 'Master\MataUangController@get_all']);
Route::get('master/mata_uang/{id}',['uses' => 'Master\MataUangController@get']);
Route::get('master/mata_uang/get_by/{param}/{value}',['uses' => 'Master\MataUangController@get_by']);
Route::get('master/mata_uang/{start}/{limit}',['uses' => 'Master\MataUangController@get_limit']);
Route::post('master/mata_uang',['uses' => 'Master\MataUangController@create']);
Route::put('master/mata_uang',['uses' => 'Master\MataUangController@update']);
Route::delete('master/mata_uang',['uses' => 'Master\MataUangController@del']);

/*TAHAPAN LELANG*/
Route::get('master/tahapan_lelang',['uses' => 'Master\TahapanLelangController@get_all']);
Route::get('master/tahapan_lelang/{id}',['uses' => 'Master\TahapanLelangController@get']);
Route::get('master/tahapan_lelang/get_by/{param}/{value}',['uses' => 'Master\TahapanLelangController@get_by']);
Route::get('master/tahapan_lelang/{start}/{limit}',['uses' => 'Master\TahapanLelangController@get_limit']);
Route::post('master/tahapan_lelang',['uses' => 'Master\TahapanLelangController@create']);
Route::put('master/tahapan_lelang',['uses' => 'Master\TahapanLelangController@update']);
Route::delete('master/tahapan_lelang',['uses' => 'Master\TahapanLelangController@del']);

/*TIPE PERUSAHAAN*/
Route::get('master/tipe_perusahaan',['uses' => 'Master\TipePerusahaanController@get_all']);
Route::get('master/tipe_perusahaan/{id}',['uses' => 'Master\TipePerusahaanController@get']);
Route::get('master/tipe_perusahaan/get_by/{param}/{value}',['uses' => 'Master\TipePerusahaanController@get_by']);
Route::get('master/tipe_perusahaan/{start}/{limit}',['uses' => 'Master\TipePerusahaanController@get_limit']);
Route::post('master/tipe_perusahaan',['uses' => 'Master\TipePerusahaanController@create']);
Route::put('master/tipe_perusahaan',['uses' => 'Master\TipePerusahaanController@update']);
Route::delete('master/tipe_perusahaan',['uses' => 'Master\TipePerusahaanController@del']);

/**ANGGARAN **/

    /*PERMINTAAN PESANAN*/
        Route::get('anggaran/permintaan_pesanan',['uses' => 'Anggaran\PermintaanPesananController@get_all']);
        Route::get('anggaran/permintaan_pesanan/{start}/{limit}',['uses' => 'Anggaran\PermintaanPesananController@get_limit']);
        Route::get('anggaran/permintaan_pesanan/{id}',['uses' => 'Anggaran\PermintaanPesananController@get']);
        Route::get('anggaran/permintaan_pesanan/get_by/{param}/{value}',['uses' => 'Anggaran\PermintaanPesananController@get_by']);
        Route::post('anggaran/permintaan_pesanan',['uses' => 'Anggaran\PermintaanPesananController@create']);
        Route::post('anggaran/permintaan_pesanan/cp',['uses' => 'Anggaran\PermintaanPesananController@moveToPengadaan']);
        Route::put('anggaran/permintaan_pesanan',['uses' => 'Anggaran\PermintaanPesananController@update']);
        Route::delete('anggaran/permintaan_pesanan',['uses' => 'Anggaran\PermintaanPesananController@del']);
    
    /*PERMINTAAN PESANAN ITEM*/    
        Route::get('anggaran/permintaan_pesanan_item',['uses' => 'Anggaran\PermintaanPesananItemController@get_all']);
        Route::get('anggaran/permintaan_pesanan_item/{start}/{limit}',['uses' => 'Anggaran\PermintaanPesananItemController@get']);
        Route::get('anggaran/permintaan_pesanan_item/{id}',['uses' => 'Anggaran\PermintaanPesananItemController@get']);
        Route::get('anggaran/permintaan_pesanan_item/get_by/{param}/{value}',['uses' => 'Anggaran\PermintaanPesananItemController@get_by']);
        Route::post('anggaran/permintaan_pesanan_item',['uses' => 'Anggaran\PermintaanPesananItemController@create']);
        Route::put('anggaran/permintaan_pesanan_item',['uses' => 'Anggaran\PermintaanPesananItemController@update']);
        Route::delete('anggaran/permintaan_pesanan_item',['uses' => 'Anggaran\PermintaanPesananItemController@del']);

    /*ANGGARAN*/
        Route::get('anggaran/anggaran',['uses' => 'Anggaran\AnggaranController@get_all']);
        Route::get('anggaran/anggaran/{start}/{limit}',['uses' => 'Anggaran\AnggaranController@get']);
        Route::get('anggaran/anggaran/{id}',['uses' => 'Anggaran\AnggaranController@get']);
        Route::get('anggaran/anggaran/get_by/{param}/{value}',['uses' => 'Anggaran\AnggaranController@get_by']);
        Route::put('anggaran/anggaran',['uses' => 'Anggaran\AnggaranController@update']);
        Route::post('anggaran/anggaran',['uses' => 'Anggaran\AnggaranController@create']);
        Route::delete('anggaran/anggaran',['uses' => 'Anggaran\AnggaranController@del']);

    /*RENCANA PENGADAAN*/
        Route::get('anggaran/rencana_pengadaan',['uses' => 'Anggaran\RencanaPengadaanController@get_all']);
        Route::get('anggaran/rencana_pengadaan/{start}/{limit}',['uses' => 'Anggaran\RencanaPengadaanController@get']);
        Route::get('anggaran/rencana_pengadaan/{id}',['uses' => 'Anggaran\RencanaPengadaanController@get']);
        Route::get('anggaran/rencana_pengadaan/get_by/{param}/{value}',['uses' => 'Anggaran\RencanaPengadaanController@get_by']);
        Route::put('anggaran/rencana_pengadaan',['uses' => 'Anggaran\RencanaPengadaanController@update']);
        Route::post('anggaran/rencana_pengadaan',['uses' => 'Anggaran\RencanaPengadaanController@create']);
        Route::delete('anggaran/rencana_pengadaan',['uses' => 'Anggaran\RencanaPengadaanController@del']);

    /*RENCANA PENGADAAN ITEM*/
        Route::get('anggaran/rencana_pengadaan_item',['uses' => 'Anggaran\RencanaPengadaanItemController@get_all']);
        Route::get('anggaran/rencana_pengadaan_item/{start}/{limit}',['uses' => 'Anggaran\RencanaPengadaanItemController@get']);
        Route::get('anggaran/rencana_pengadaan_item/{id}',['uses' => 'Anggaran\RencanaPengadaanItemController@get']);
        Route::get('anggaran/rencana_pengadaan_item/get_by/{param}/{value}',['uses' => 'Anggaran\RencanaPengadaanController@get_by']);
        Route::put('anggaran/rencana_pengadaan_item',['uses' => 'Anggaran\RencanaPengadaanItemController@update']);
        Route::post('anggaran/rencana_pengadaan_item',['uses' => 'Anggaran\RencanaPengadaanItemController@create']);
        Route::delete('anggaran/rencana_pengadaan_item',['uses' => 'Anggaran\RencanaPengadaanItemController@del']);

/** VENDOR MANAGEMENT **/

    Route::post('vendor/reg', ['uses' => 'Vendor\VendorController@insertLocal']);
    Route::get('vendor/needapp', ['uses' => 'Vendor\VendorController@getNeedApproval']);
    Route::get('vendor/needapp/org/{id}', ['uses' => 'Vendor\VendorController@getVendorByOrg']);
    Route::get('vendor/needapp/{id}', ['uses' => 'Vendor\VendorController@getObjectDataByVendor']);
    Route::post('vendor/needapp/app', ['uses' => 'Vendor\VendorController@approveVendor']);
    Route::get('vendor/needapp/view/class/{id}', ['uses' => 'Vendor\VendorController@getVendorClassification']);
    Route::put('vendor/needapp/view/class', ['uses' => 'Vendor\VendorController@updateClassification']);
    Route::get('vendor/needapp/view/doc/{id}', ['uses' => 'Vendor\VendorController@getVendorDocuments']);
    Route::post('vendor/needapp/view/doc', ['uses' => 'Vendor\VendorController@updateDocumentVendor']);
    Route::put('vendor/needapp', ['uses' => 'Vendor\VendorController@updateVendorLokal']);
    Route::get('vendor/active', ['uses' => 'Vendor\VendorController@getAllAcceptedApproval']);
    Route::post('vendor/active/org/{id}', ['uses' => 'Vendor\VendorController@getVendorByOrg']);
    Route::post('vendor/active/limit/{start}/{limit}', ['uses' => 'Vendor\VendorController@create']);
    Route::post('vendor/active/org/limit/{start}/{limit}', ['uses' => 'Vendor\VendorController@create']);
    Route::post('vendor/active/{id}', ['uses' => 'Vendor\VendorController@create']);
    Route::put('vendor/active/', ['uses' => 'Vendor\VendorController@suspendVendorLokal']);
    Route::get('vendor/rejected', ['uses' => 'Vendor\VendorController@getRejectedVendorLokal']);
    Route::get('vendor/tesss', ['uses' => 'Vendor\VendorController@getActiveVendor']);

    Route::get('vendor', ['uses' => 'Vendor\VendorController@tes']);
    Route::post('vendor', ['uses' => 'Vendor\VendorController@save']);
    Route::post('vendor/create', ['uses' => 'Vendor\VendorController@insertLocal']);
    Route::get('vendor/arr', ['uses' => 'Vendor\VendorController@get_arr']);
    Route::get('vendor/approval', ['uses' => 'Vendor\VendorController@getAllAcceptedApproval']);
    Route::get('vendor/list_need_approval', ['uses' => 'Vendor\VendorController@getNeedApproval']);
    Route::post('vendor/setting/', ['uses' => 'Vendor\VendorController@approveVendor']);
    Route::get('vendor/status/{id}', ['uses' => 'Vendor\VendorController@getStatusByVendor']);
    

/** TENDER MANAGEMENT **/
    
    Route::get('lelang', ['uses' => 'Tender\TenderController@getActiveLelang']);
    Route::post('lelang', ['uses' => 'Tender\TenderController@createLelang']);
    Route::delete('lelang', ['uses' => 'Tender\TenderController@delLelang']);
    Route::put('lelang', ['uses' => 'Tender\TenderController@updateLelang']);
    Route::put('lelang/submit', ['uses' => 'Tender\TenderController@submitLelang']);
    
    /*
        Route::get('lelang', ['uses' => 'Tender\TenderController@getActiveLelang']);
        Route::get('lelang/org/{id}', ['uses' => 'Tender\TenderController@getActiveLelangByOrg']);
        Route::get('lelang/limited', ['uses' => 'Tender\TenderController@getActiveLelangLimit']);
        Route::get('lelang/org/limited', ['uses' => 'Tender\TenderController@getActiveLelangByOrgLimit']);
        Route::get('lelang/{id}', ['uses' => 'Tender\TenderController@getLelangById']);
        
        
        Route::put('lelang/submit', ['uses' => 'Tender\TenderController@submitLelang']);
        
        Route::put('lelang/approval', ['uses' => 'Tender\TenderController@approveLelang']);
        lelang/broadcast
        lelang/broadcast
        lelang/view/vendor/{id}
        lelang/vendor
        lelang/vendor
        lelang/views/sch/{id}
        lelang/view/sch
        lelang/view/sch
        lelang/view/assesment/{id}
        lelang/view/assesment
        lelang/view/assesment
        lelang/view/assesment/detail/{id}
        lelang/view/assesment/detail
        lelang/view/assesment/detail
        lelang/view/assesment/detail
        lelang/view/doc/{id}
        lelang/view/doc
        lelang/view/doc
        lelang/view/doc
        lelang/ongoing
        lelang/vendor/kualifikasi/{id}
        lelang/vendor/kualifikasi/detail/{id_lelang}/{id_rekanan}
        lelang/vendor/kualifikasi/approval
        lelan/ongoing
        lelang/aanwijzing/{id}
        lelang/aanwijzing/komentar/{id}
        lelang/aanwijzing/komentar/quote/{id}
        lelang/aanwijzing/ts
        lelang/aanwijzing/dokumen
        lelang/aanwijzing/komentar
        lelang/ongoing
        lelang/vendor/penawaran/{id}
        lelang/vendor/penawaran/item/{id_lelang}/{id_rekanan}
        lelang/vendor/penawaran/dokumen/{id_lelang}/{id_rekanan}
        lelang/vendor/penawaran/approval
        lelang/ongoing
        lelang/vendor/eauction/{id}
        lelang/vendor/eauction/item/{id_lelang}/{id_rekanan}
    */
   
Route::module('contoh');

Route::module('development');

/** USER MANAGEMENT */
    Route::post('register', 'AuthController@register');
    Route::get('user/verify/{verification_code}', 'AuthController@verifyUser');
    Route::post('login', 'AuthController@login');
    Route::post('logout', 'AuthController@logout');
    Route::post('recover', 'AuthController@recover');

// Route::post('user','UserController@profile')->middleware('authgw');
// Route::post('user/roles', ['middleware' => ['roles:users'], 'uses' => 'UserController@create']);
Route::post('user/el','UserController@read_eloquent'); 
Route::post('user/ability', 
    [
    'middleware' => ['abilities:admin|user,read-profile|edit-user,require_all'],
    'uses' => 'UserController@ability'
]);
Route::post('user/permissiontest', 
    [
    'middleware' => ['permissions:read-profile|edit-post'],
    'uses' => 'UserController@ability'
]);

Route::post('user/log', 'UserController@log');
Route::post('user/create', 'UserController@create');
Route::get('coba', 'UserController@coba');
Route::get('get_token', 'UserController@get_token');

/**
* bentuk standar api
*
* get 
* create
* create_by
*       excel
*       csv
* update
* delete
*   delete_bulk 
*/
Route::get('/get/{hashed_id}', 'UserController@get');

//user
Route::group(['prefix' => 'users', 'middleware' => ['permissions:read-users']], function() {
    Route::post('/get/{hashed_id}', 'UserController@get');
    Route::post('/create', [
            'middleware' => ['permissions:create-users,require_all'], 
            'uses' => 'UserController@create'
        ]);
    Route::post('/createByCsv/', [
            'middleware' => ['permissions:create-users'], 
            'uses' => 'UserController@createByCsv'
        ]);
    Route::post('/update', [
            'middleware' => ['permissions:update-users'],
            'uses' => 'UserController@update'
        ]);
    Route::post('/delete/{hashed_id}', [
            'middleware' => [],
            'uses' => 'UserController@del'
        ]);
    Route::post('/delete_bulk',[
            'middleware' => ['permissions:delete-users'],
            'uses' => 'UserController@del_bulk'
        ]);
    Route::post('/get_info', 'UserController@get_info');

});

Route::post('/delete/{hashed_id}', [
            'middleware' => [],
            'uses' => 'UserController@del'
        ]);

Route::post('/update', [
            'middleware' => [],
            'uses' => 'UserController@update'
        ]);

//master province 
/*
Route::group(['prefix' => 'master/province','middleware' => ['permissions:read-provinces']], function() {
    Route::post('/create', [
            'middleware' => ['permissions:create-provinces'], 
            'uses' => 'ProvinceController@create'
        ]);





    Route::post('/createByCsv/', [
            'middleware' => ['permissions:create-users'], 
            'uses' => 'ProvinceController@createByCsv'
        ]);
    Route::post('/update', [
            'middleware' => ['permissions:update-users'],
            'uses' => 'ProvinceController@update'
        ]);
    Route::post('/delete/{hashed_id}', [
            'middleware' => [],
            'uses' => 'ProvinceController@del'
        ]);
    Route::post('/delete_bulk',[
            'middleware' => ['permissions:delete-users'],
            'uses' => 'ProvinceController@del_bulk'
        ]);
    Route::post('/get_info', 'UserController@get_info');

});
*/

//master city
Route::post('city','Master\CityController@get');

//roles
Route::post('roles/create',['middleware' => ['permissions:create-acl'],
    'uses' => 'RoleController@create'
]); 

Route::post('user/get_by/{param}','UserController@get_by'); 


Route::post('mongo', function(Request $request) {
    $collection = \Mongo::get()->Log->collection;
    return $collection->find()->toArray();
});

Route::post('mongolog', function(Request $request) {
    dd(getUser());
    dd(fooBar());
    $collection = new Log();
    dd($collection->cari());
    // return $collection->find()->toArray();
});

//permissions

Route::post('upload', 'UserController@upload');

Route::get('form_upload', 'UserController@form_upload');

Route::get('trait', 'Master\KotaController@coba');
Route::get('trait2', 'Master\KotaController@printThis');
Route::get('trait3', 'Master\KotaController@getX');
// Route::post('province','Master\ProvinceController@create');
// Route::post('province/get/{start?}/{limit?}','Master\ProvinceController@get');
Route::get('hash/{id}','UserController@hashed');
Route::get('unhash/{hashed_id}','UserController@unhashed');

Route::get('tes123', 'Master\BidangUsahaController@tes');