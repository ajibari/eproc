var express = require('express');
var router = express.Router();
var helper = require('../helper');

/* GET users listing. */
router.get('/tesemail', function(req, res, next) {
  res.send('respond with a resource email');
});

router.get('/sendemail', function(req, res, next) {

	var dataTemplate = {
	  "body": 'practical node.js',
	  "author": '@azat_co',
	  "tags": ['express', 'node', 'javascript']
	}

    var emailOption = {
      "sender": "'Ridwan Aji Bari' <bari@gmail.com>",
      "receivers": [
        "tes@gmail.com",
        "tes2@gmail.com",
        "tes3@gmail.com"
      ],
      "subject": "Tes email",
      "data": dataTemplate,
      "attachments": [{   
        // utf-8 string as an attachment
                filename: 'text1.txt',
                content: 'hello world!'
          }],
          "template": "template1"
    }

    helper.EmailSend(emailOption, function(result){
      console.log(result);
      process.exit(0);
    });

});

module.exports = router;
