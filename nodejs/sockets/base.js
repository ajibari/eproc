var Schema = require('./../schema');

module.exports = function (io, helper) { 

	var userid = [];

	//initialize IO connection
	io.on('connection', function(socket){
	  console.log('user terhubung.');

	  socket.on('channel:chat_user', function(data, callback){
	  	//for register user in chat
	  	if(userid.indexOf(data) < 0){
	  		socket.nickname = data;
	  		userid.push(socket.nickname);
	  	} else{
	  		return false;
	  	}

	  });

	  socket.on('channel:chat', function(pesan) {
	  	//channel realtime chat
	  	var datajson = {"user_id": 2, "channel_id": "123123", "session_socket" : "123apaan", "message" : pesan};
		helper.DBCreate(datajson, "channel_log", Schema.channel_logSchema);

	    io.emit('channel:chat', pesan);
	    console.log(pesan);
	  });

	  socket.on('disconnect', function(){
	    console.log('user terputus.');
	  });
	  
	});
}