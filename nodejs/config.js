var config = {
	email: {
		host: 'localhost',
		port: 1025,
		secure: false,
		auth: {
			user: null,
			pass: null
		}
	},
	dbPostgre: {
		user: 'postgres',
		host: 'localhost',
		database: 'db_eproc',
		password: 'admin123',
		port: 5432
	}
};

module.exports = config;