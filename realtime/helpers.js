var mongoose = require('mongoose');
var mongoDb = 'mongodb://127.0.0.1/my_database';
var mongoQueueDb = 'mongodb://127.0.0.1/task_queue';
var mongodb = require('mongodb');
var mongoDbQueue = require('mongodb-queue');
var monq = require('monq');
var monqDb = monq('mongodb://localhost:27017/queue_job');

var moment = require('moment');
moment().format();

mongoose.connect(mongoDb, { useMongoClient: true });
mongoose.Promise = global.Promise;




var exports = module.exports = {};

exports.DBCreate = function(data, collection, schema){
 	var Model = mongoose.model(collection, schema);
 	channel = new Model(data);

 	channel.save(function(err){
 		console.log("data ditambahkan disimpan di "+collection);

 		if(err){
 			console.log(err);
 		}
 	});
}

exports.DBUpdate = function(data, param, value, collection, schema){
 	var Model = mongoose.model(collection, schema);
 	var cond = '{'
       +'"'+param+'" : "'+value+'"'
       +'}';

 	var query = JSON.parse(cond);
 	var update = {
            "$set": data 
        };
    var options = { "multi": true };

 	Model.update(query, update, options, function (err) {
 		console.log("data perubahan disimpan di "+collection);
        if (err) return console.error(err);         
    })
}

exports.DBDelete = function(param, value, collection, schema){
 	var Model = mongoose.model(collection, schema);
 	var cond = '{'
       +'"'+param+'" : "'+value+'"'
       +'}';
       
 	var query = JSON.parse(cond);

 	Model.remove(query, function(err){
 		console.log("data penghapusan di "+collection);
 		if(err) return console.error(err);
 	});
}

exports.DBGet = function(param, collection, schema, callback){
	var Model = mongoose.model(collection, schema);

 	Model.find(param, function(err, result){
 		
 		return callback(result);

 		if(err) return console.error(err);

 	});
}

exports.QueueAdd = function(task, callback){
    
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth()+1; //January is 0!
    var yyyy = today.getFullYear();

    if(dd<10) {
        dd = '0'+dd
    } 

    if(mm<10) {
        mm = '0'+mm
    } 

    today = yyyy + '_' + mm + '_' + dd;

    mongodb.MongoClient.connect(mongoQueueDb, function(err, db) {
      var queue = mongoDbQueue(db, 'TASK_'+today);

      queue.add(task, function(err, id) {
          return callback(id);
      });
    })

}

exports.QueueGet = function(callback){
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth()+1; //January is 0!
    var yyyy = today.getFullYear();

    if(dd<10) {
        dd = '0'+dd
    } 

    if(mm<10) {
        mm = '0'+mm
    } 

    today = yyyy + '_' + mm + '_' + dd;

    mongodb.MongoClient.connect(mongoQueueDb, function(err, db) {
    
      var queue = mongoDbQueue(db, 'TASK_'+today);

      queue.get(function(err, msg, callback) {
        // console.log('msg.id=' + msg.id)
        // console.log('msg.ack=' + msg.ack)
        // console.log('msg.payload=' + msg.payload) // 'Hello, World!' 
        // console.log('msg.tries=' + msg.tries)
        console.log(msg);
        // return callback(msg);
      });

    });
    
}

exports.MomentFromUtc = function(time, callback){

  var date = moment(time);
  var dateComponent = date.utc().format('YYYY-MM-DD');
  var timeComponent = date.utc().format('HH:mm:ss');

  return callback(dateComponent+ ' ' +timeComponent);
}

exports.MomentToUtc = function(time, callback){
  //time format = YYYY-MM-DD H:i:s
  return callback(moment(time).toISOString());
}

exports.JobAdd = function(name, job, callback){
  var queue = monqDb.queue(job);

  queue.enqueue(name, { text: 'foobar' }, function (err, job) {
      console.log('enqueued:', job);
  });

}

exports.JobStart = function(name, job, callback){
  var worker = monqDb.worker(job);
  var job_function = {
    cobaemail: function (params, callback) {
        try {
              var reversed = params.text.split('').reverse().join('');
              callback(null, reversed);
          } catch (err) {
              callback(err);
          }
      }
  };

  worker.register(job_function);
  worker.start();

  worker.on('complete', function (data) {
    console.log(data.result);
  });
}

exports.getDateTime = function(){
  var date = new Date();

  var hour = date.getHours();
  hour = (hour < 10 ? "0" : "") + hour;

  var min  = date.getMinutes();
  min = (min < 10 ? "0" : "") + min;

  var sec  = date.getSeconds();
  sec = (sec < 10 ? "0" : "") + sec;

  var year = date.getFullYear();

  var month = date.getMonth() + 1;
  month = (month < 10 ? "0" : "") + month;

  var day  = date.getDate();
  day = (day < 10 ? "0" : "") + day;

  return year + ":" + month + ":" + day + ":" + hour + ":" + min + ":" + sec;  
}