var monq = require('monq');
var client = monq('mongodb://localhost:27017/monq_example');
var Helper = require('./helpers');
var queue = client.queue('example');

var today = new Date();
var dd = today.getDate();
var mm = today.getMonth()+1; //January is 0!
var yyyy = today.getFullYear();

if(dd<10) {
    dd = '0'+dd
} 

if(mm<10) {
    mm = '0'+mm
} 

today = yyyy + '_' + mm + '_' + dd;


queue.enqueue('sendEmail', { text: 'foobar' }, function (err, job) {
    console.log('enqueued:', job);
});

var worker = client.worker('example');

worker.register({
    sendEmail: function (params, callback) {
        try {
            var reversed = params.text.split('').reverse().join('');
            callback(null, reversed);
        } catch (err) {
            callback(err);
        }
    }
});

worker.start();

worker.on('complete', function (data) {
	console.log(data.result);
});

Helper.MomentToUtc("2012-01-01 10:00:09", function(result){
	// console.log(result);
})