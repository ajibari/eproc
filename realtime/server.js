var config = require('./config.js').get(process.env.NODE_ENV);

var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);
var formidable = require('express-formidable');
var morgan = require('morgan');
var fs = require('fs');
var mongoMorgan = require('mongo-morgan');
var mongoDbQueue = require('mongodb-queue');

var accessLogStream = fs.createWriteStream(__dirname + '/access_log.log', {flags: 'a'});

var Schema = require('./schema');
var Helper = require('./helpers');
var EngineEmail = require('./engine_email');

var socketid = [];




// console.log(config.database);

/** 
 * [contoh save data to mongodb]
 * @type {Object}
 
var datajson = {"user_id": 2, "channel_id": "123123", "session_socket" : "123apaan"};
Helper.DBCreate(datajson, "channel_log", Schema.channel_logSchema);
Helper.DBUpdate(datajson, "user_id", 4, "channel_log", Schema.channel_logSchema);
Helper.DBDelete("user_id",1,"channel_log", Schema.channel_logSchema);

var datajson = {"user_id": 2};
Helper.DBGet(datajson,"channel_log", Schema.channel_logSchema, function(result){
	var hasil = result;
	console.log(hasil);
});
*/

/**
app.get();
app.post();
app.put();
app.delete();
more https://expressjs.com/en/api.html#app.METHOD
*/

/**
MIDDLEWARE
*/

const urlLogger = (request, response, next) => {
  console.log('Request URL:', request.url);
  next();
};

const timeLogger = (request, response, next) => {
  console.log('Datetime:', new Date(Date.now()).toString());
  next();
};

const DbLogger = (request, response, next) => {
	Helper.DBCreate(datajson, "channel_log", Schema.channel_logSchema);
};

app.use(morgan('combined', {stream: accessLogStream}));


var routeLogger = function (req, res, next) {

	datajson = {user_id: 1, url: req.originalUrl, method: req.method, ip: req.ip, response: 'response'};
	Helper.DBCreate(datajson, "log", Schema.logSchema);
  	next();
}


app.use(routeLogger);
app.use(formidable());

/**
END OF MIDDLEWARE
*/

/**
ROUTE
*/

app.get('/', function(req, res) {

  // res.sendFile(__dirname + '/chat/index.html');
  res.send('tes');

});

app.post('/channel_pesan', (req, res) => {

	var msgPesan;
	msgPesan = req.fields;

	if(typeof msgPesan['message'] != 'undefined'){
		
		if(msgPesan['message'].length > 0){

			//send to websocket
				io.emit('channel_pesan',req.fields['message']);
				res.send(true);	
				return;
		}
		
	}

	res.send(false);

});

app.get('/insert_json', (req, res) => {

	//insert json into 1 kolom

	var http = require('http');

	var options = {
	  host: 'localhost',
	  port: '8000',
	  path: '/api/master/pengguna/all'
	};

	callback = function(response) {
	  var str = '';

	  //another chunk of data has been recieved, so append it to `str`
	  response.on('data', function (chunk) {
	    str += chunk;
	  });

	  //the whole response has been recieved, so we just print it out here
	  response.on('end', function () {
	  	var result = JSON.parse(str);
	    var datajson = {"data" : result.Response};
		Helper.DBCreate(datajson, "channel_coba", Schema.channel_cobaSchema);

	  });
	}

	http.request(options, callback).end();
	//Helper.DBCreate(datajson, "channel_log", Schema.channel_logSchema);
});

app.get('/json_db', (req, res) => {
	
	//get json from mongodb, tipe data di db adalah json
	
	var datajson = {"__v": 0};

	Helper.DBGet(datajson,"channel_cobas", Schema.channel_cobaSchema, function(result){
		var hasil = result;
		console.log(hasil[0].data[0]);
		console.log(typeof hasil);
	});
});

app.get('/message_queue', (req, res) => {


	Helper.QueueAdd('helloworld1234', function(result){
		console.log(result);
	});
});

app.get('/message_get', (req, res) => {
	Helper.QueueGet();
});

app.post('/post_job', (req, res) => {
	var job, jobTask;
	job = req.fields;

	if(job['task'] == 'sending email'){
		jobTask = 'sendemail';
	}

	Helper.JobAdd(job['name'], jobTask);
})

app.post('/start_job', (req, res) => {
	var job, jobTask;
	job = req.fields;

	Helper.JobStart(job['name'], job['task']);
});

//initialize IO connection
	io.on('connection', function(socket){

		socketid.push(socket.id);
		
		socket.on('channel_pesan', function(msg){
			io.emit('channel_pesan', msg);
		});

		socket.on('channel:notification', function(msg){
			io.emit('channel:notification', msg);
		});

		socket.on('disconnect', function (data) {
			console.log('client disconnected');
		});

	});

/**
 * Sending Email example

	var dataTemplate = {
	  "body": 'practical node.js',
	  "author": '@azat_co',
	  "tags": ['express', 'node', 'javascript']
	}

	var emailOption = {
		 		"sender": "'Ridwan Aji Bari' <bari@gmail.com>",
		 		"receivers": [
		 			"tes@gmail.com",
		 			"tes2@gmail.com",
		 			"tes3@gmail.com"
		 		],
		 		"subject": "Tes email",
		 		"data": dataTemplate,
		 		"attachments": [{   
		 			// utf-8 string as an attachment
		            	filename: 'text1.txt',
		            	content: 'hello world!'
		        }],
		        "template": "template1"
			}

	EngineEmail.sendEmail(emailOption, function(result){
		console.log(result);
	});

*/

app.get('/json', urlLogger, timeLogger, (request, response) => {
  response.status(200).json({"name": "Robbie"});
});

/**
END ROUTE
*/

app.listen(3000, () => {
  console.log('Express intro running on localhost:3000 '+Helper.getDateTime());
});

