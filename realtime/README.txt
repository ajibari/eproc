format struktur table realtime mongodb
======================================
channel
	id
	name
	created_by
	created_at
	modified_by
	modified_at
	
channel_member
	user_id	
	channel_id
	created_at

channel_konten
	channel_id
		channel_id => 
			'pesan' => [
				['user_id' => 1, 'message' => message_content],
				['user_id' => 2, 'message' => message_content],
			]
	created_at
	
channel_log
	user_id
	channel_id
	session_socket
	created_at
