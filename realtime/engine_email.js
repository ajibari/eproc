var config = require('./config.js').get(process.env.NODE_ENV);
var nodemailer = require('nodemailer');
var fs = require('fs');
var handlebars = require('handlebars');

var exports = module.exports = {};

exports.sendEmail = function(emailOption, callback){

	//read template first
		fs.readFile('realtime/template/email/' + emailOption.template +'.tbs', 'utf-8', function(error, source){
			
			if (error) {
		    	return console.log(error);
		  	}

		  	//render with handlebars
				var template = handlebars.compile(source);
				var templateHtml = template(emailOption.data);

			//sending email
				let transporter = nodemailer.createTransport({
			        host: config.email.host,
			        port: config.email.port,
			        secure: config.email.secure, // true for 465, false for other ports
			        tls: {
				        rejectUnauthorized: false
				    }
			    });

			    let mailOptions = {
			        from: emailOption.sender, // sender address
			        to: emailOption.receivers, // list of receivers
			        subject: emailOption.subject, // Subject line
			        html: templateHtml, // html body
			        attachments: emailOption.attachments
			    };

			    transporter.sendMail(mailOptions, (error, info) => { 
			        if (error) {
			            return console.log(error);
			        }

			        console.log('Message sent: %s', info.messageId);
			        // Preview only available when sending through an Ethereal account
			        console.log('Preview URL: %s', nodemailer.getTestMessageUrl(info));

			        var result = true;
					
					return callback(result);        
			    });
		});
		
	

}