var mongoose = require("mongoose");
var Schema = mongoose.Schema;

// schema Channel log
var channel_logSchema = new Schema({
		user_id: Number,
		channel_id: String,
		session_socket: String,
		created_at: {type: Date, default: Date.now}
});

//schema Channel
var channel = new Schema({
	channel_id: String,
	name: String,
	created_by: Number,
	created_at: {type: Date, default: Date.now},
	modified_by: Number,
	modified_at: {type: Date, default: Date.now}
});

//schema Channel content
var channel_contentSchema = new Schema({
		channel_id: Array,
		created_at: {type: Date, default: Date.now}
});

//schema Channel member
var channel_memberSchema = new Schema({
	user_id: Number,
	channel_id: String,
	created_at: {type: Date, default: Date.now}
});

//schema Log 
var logSchema = new Schema({
	user_id: Number,
	url: String,
	method: String,
	ip: String,
	response: String,
	created_at: {type: Date, default: Date.now}
});

//schema Token User
var tokenSchema = new Schema({
	user_id: Number,
	token: String,
	modified_at: {type: Date, default: null},
	created_at: {type: Date, default: Date.now}
});

var channel_cobaSchema = new Schema({
	data: Object,
	created_at: {type: Date, default: Date.now}
});

var channel_TaskSchema = new Schema({
	task: String,
	created_at: {type: Date, default: Date.now}
});

//export all schema
module.exports = {
  channel_logSchema : channel_logSchema,
  channel : channel,
  channel_contentSchema : channel_contentSchema,
  channel_memberSchema : channel_memberSchema,
  logSchema : logSchema,
  tokenSchema: tokenSchema,
  channel_cobaSchema: channel_cobaSchema,
  channel_TaskSchema: channel_TaskSchema
}