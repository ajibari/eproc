var config = {
  production: {
    email:{
      host: 'smtp.ethereal.email',
      port: 587,
      secure: false, // true for 465, false for other ports
      auth: {
          user: null, // generated ethereal user
          pass: null  // generated ethereal password
      }
    },
    session: {
      key: 'the.express.session.id',
      secret: 'something.super.secret'
    },
    database: 'mongodb://<user>:<pwd>@apollo.modulusmongo.net:27017/db',
    twitter: {
      consumerKey: 'consumer Key',
      consumerSecret: 'consumer Secret',
      callbackURL: 'http://yoururl.com/auth/twitter/callback'
    }
  },
  development: {
    email:{
      host: 'localhost',
        port: 1025,
        secure: false, // true for 465, false for other ports
        auth: {
            user: null, // generated ethereal user
            pass: null  // generated ethereal password
        }
    },
    session: {
      key: 'the.express.session.id',
      secret: 'something.super.secret'
    },
    database: 'mongodb://127.0.0.1:27017/development',
    twitter: {
      consumerKey: 'consumer Key',
      consumerSecret: 'consumer Secret',
      callbackURL: 'http://127.0.0.1:3000/auth/twitter/callback'
    }
  }
}

exports.get = function get(env) {
  return config[env] || config.default;
}