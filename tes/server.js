const express = require('express');
const app = express();

/**
app.get();
app.post();
app.put();
app.delete();
more https://expressjs.com/en/api.html#app.METHOD
*/

/**
MIDDLEWARE
*/

const urlLogger = (request, response, next) => {
  console.log('Request URL:', request.url);
  next();
};

const timeLogger = (request, response, next) => {
  console.log('Datetime:', new Date(Date.now()).toString());
  next();
};

/**
END OF MIDDLEWARE
*/

/**
ROUTE
*/

app.get('/', function(req, res) {

  res.sendFile(__dirname + '/chat/index.html');

});

app.get('/json', urlLogger, timeLogger, (request, response) => {
  response.status(200).json({"name": "Robbie"});
});

/**
END ROUTE
*/

app.listen(3000, () => {
  console.log('Express intro running on localhost:3000');
});