'use strict';

var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);
var path = require('path');

app.set('port', process.env.PORT || 8181);

io.on('connection', function(socket) {
  socket.on('notif:pesan', function(pesan) {
    io.emit('notif:pesan', pesan);
  });
});

http.listen(app.get('port'), function() {
  console.log('Server jalan di port ' + app.get('port'));
});