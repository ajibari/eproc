var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);
var formidable = require('express-formidable');
var mongoose = require('mongoose');
var mongoDB = 'mongodb://127.0.0.1/my_database';
var socketid = [];
/* MIDDLEWARE */
	
	//read form data
		app.use(formidable());

	//connect to Mongo
		var mongoose = require('mongoose');
		mongoose.connect(mongoDB, { useMongoClient: true });
		mongoose.Promise = global.Promise;

/* END OF MIDDLEWARE */

app.get('/', function(req, res){
  res.sendFile(__dirname + '/chat/index.html');
});

app.get('/index2', function(req, res){
	res.sendFile(__dirname + '/chat/index2.html');
});

app.get('/pesan', function(req, res){
	res.sendFile(__dirname + '/chat/channel-pesan.html');
});

app.post('/create-channel', (req, res) => {
	res.send(req.fields)
	return true;
})


app.post('/channel_pesan', (req, res) => {

	var msgPesan;
	msgPesan = req.fields;

	if(typeof msgPesan['message'] != 'undefined'){
		
		if(msgPesan['message'].length > 0){

			//send to websocket
				io.emit('channel:pesan',req.fields['message']);
				res.send(true);	
				return;
			//send specific
				// io.sockets.connected[socketid[1]].emit('message', 'some text for you only');
				// console.log(socketid[1]);
				// socket = io.connect();  
				// //io.on('connection', (socket, id, msg) => {
				// //	socket.on('channel:pesan', function(msg){
				// 		io.emit('channel:pesan',socketid[1]);

				//     	socket.to(socketid[1]).emit('message', 'message ini gw');

				    	// let namespace = null;
					    // let ns = io.of(namespace || "/");
					    // console.log(socketid[0]);
					    // let socket = ns.connected[socketid[0]] // assuming you have  id of the socket
					    // if (socket) {
					    //     console.log("Socket Connected, sent through socket");
					    //     // socket.emit('message', "tes data private");
					    //     socket.broadcast.to(socketid[0]).emit('message', 'for your eyes only');
					    // } else {
					    //     console.log("Socket not connected, sending through push notification");
					    // }
						// io.to(socketid[0]).emit("message", "tes kirim data");
						// io.sockets.socket(socketid[0]).emit('message','tes kirim pesan'); 
				//	});
				//});
		}
		
	}

	res.send(false);

});

app.post('/channel-auction', (req, res) => {

});

app.post('/channel-aanwizjing', (req, res) => {

});

app.post('/channel-pengumuman', (req, res) => {

});

app.post('/channel-bid', (req, res) => {

});




io.on('connection', function(socket){
	// socket.broadcast.emit('user connected');
	console.log(socket.id);
	socketid.push(socket.id);
	// console.log(socketid[0]);
	// socket.emit('channel:pesan', 'tessssssa');
	// socket.on('channel:pesan', function(msg){
	// 	io.emit('channel:pesan', msg);
	// });

	socket.on('channel:chat', function(msg){
		io.emit('channel:chat', msg);
	});

	socket.on('channel:notification', function(msg){
		io.emit('channel:notification', msg);
	});

	socket.on('disconnect', function (data) {
		console.log('client disconnected');
	});

});


var clients = [];

http.listen(3000, function(){
  console.log('listening on *:3000');
});
